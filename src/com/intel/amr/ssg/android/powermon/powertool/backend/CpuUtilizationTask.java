package com.intel.amr.ssg.android.powermon.powertool.backend;

import com.asksven.andoid.common.contrib.Util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * User: Marcelo Martins <martins@cs.brown.edu>
 * Date: 12/30/13
 * Time: 10:52 AM
 */
public class CpuUtilizationTask implements Callable<String[]> {
    private String outpath;
    private String[] output;
    private int duration;

    /**
     * Set thread duration
     *
     * @param duration thread duration in s
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }

    /**
     * Set filepath of computation output
     *
     * @param filename output file path
     */
    public void setOutputFile(String filename) {
        this.outpath = filename;
    }

    /**
     * Grab and return list of current user wakelocks with associated info
     *
     * @return list of user wakelocks
     * @throws Exception
     */
    public String[] call() throws Exception {
        // XXX: print top 20 processes sorted by CPU utilization
        String command = "top -n 1 -d %d -s cpu -m 20";
        List<String> res = Util.run(String.format(command, duration));
        output = res.toArray(new String[res.size()]);
        postProcess();
        return output;
    }

    /**
     * Save user wakelocks to file
     */
    void postProcess() {
        String separator = System.getProperty("line.separator");
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(new
                    File(outpath)));
            for (String line : output)
                out.write(line);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
