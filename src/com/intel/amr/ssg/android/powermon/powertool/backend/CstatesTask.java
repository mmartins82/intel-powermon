package com.intel.amr.ssg.android.powermon.powertool.backend;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * User: Marcelo Martins <martins@cs.brown.edu>
 * Date: 1/3/14
 * Time: 3:54 PM
 */
public class CstatesTask implements Runnable {
    public void run() {
        ArrayList<ArrayList<String>> summaryCores = new
                ArrayList<ArrayList<String>>();

        for (int i = 0; i < DeviceInfo.coreList.size(); i++) {
            for (int j = 0; j < DeviceInfo.coreList.get(i); j++) {
                ArrayList<String> desc = new ArrayList<String>();
                String core = "Core" + i;
                String state = "State" + j;
                desc.add(core);
                desc.add(state);
                summaryCores.add(desc);
            }
        }

        ArrayList<String> associateCores = new ArrayList<String>();
        ArrayList<String> associateUsage = new ArrayList<String>();
        ArrayList<ArrayList<String>> differenceCores = new
                ArrayList<ArrayList<String>>();
        // ArrayList<ArrayList<String>> differenceUsage = new
        // ArrayList<ArrayList<String>>();
        // ArrayList<ArrayList<String>> summaryCores = new
        // ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> measurementsBeforeRun =
                getCstatesStats(DeviceInfo.coreList);

        int theTime = 0;
        while (DeviceInfo.globalCstatesTime == 0
                || (DeviceInfo.isAllTests && DeviceInfo.globalAllTestsTime == 0)) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            theTime++;
        }

        ArrayList<ArrayList<String>> measurementsAfterRun =
                getCstatesStats(DeviceInfo.coreList);

        for (int i = 0; i < measurementsBeforeRun.size(); i++) {
            ArrayList<String> currentDifferenceRow = new ArrayList<String>();

            double timeDifference =
                    Double.parseDouble(measurementsAfterRun.get(i).get(0))
                            - Double.parseDouble(measurementsBeforeRun.get(i).get(0));
            double usageDifference =
                    Double.parseDouble(measurementsAfterRun.get(i).get(1))
                            - Double.parseDouble(measurementsBeforeRun.get(i).get(1));

            currentDifferenceRow.add(((Double) timeDifference).toString());
            currentDifferenceRow.add(((Double) usageDifference).toString());
            differenceCores.add(currentDifferenceRow);
        }

        for (int i = 0; i < differenceCores.size(); i++) {
            associateCores.add(differenceCores.get(i).get(0));
            associateUsage.add(differenceCores.get(i).get(1));
        }

        // change micro seconds to milli seconds

        for (int i = 0; i < associateCores.size(); i++) {
            Double milliseconds = Double.parseDouble(associateCores.get(i)) /
                    1000;
            associateCores.set(i, milliseconds.toString());
        }

        // calculate time in state0
        int numberOfStates = DeviceInfo.coreList.get(0);

        for (int i = 0; i < DeviceInfo.coreList.size(); i++) {
            int currentRowNumber = i * numberOfStates;

            Double sumOfOtherStates = 0.0;

            for (int j = 1; j < numberOfStates; j++) {
                int k = (numberOfStates * i) + j;
                Double currentStateTime =
                        Double.parseDouble(associateCores.get(k));
                sumOfOtherStates += currentStateTime;
            }
            Double state0Time = DeviceInfo.totalCstatesDuration - sumOfOtherStates;
            associateCores.set(currentRowNumber, state0Time.toString());
        }

        for (int i = 0; i < summaryCores.size(); i++) {
            summaryCores.get(i).add(associateCores.get(i));
            summaryCores.get(i).add(associateUsage.get(i));
            Double timeInState = Double.parseDouble(associateCores.get(i));
            Double numberOfTransitions =
                    Double.parseDouble(associateUsage.get(i));
            Double percentageInState = (timeInState /
                    DeviceInfo.totalCstatesDuration) * 100;
            Double transitionsPerSecond = numberOfTransitions * 1000
                    / DeviceInfo.totalCstatesDuration;

            summaryCores.get(i).add(percentageInState.toString());
            summaryCores.get(i).add(transitionsPerSecond.toString());
        }

        try {
            BufferedWriter
                    cStatesPen = new BufferedWriter(new FileWriter(new File(
                    DeviceInfo.pathToCstatesFile)));

            DeviceInfo.getCpuCoresInfoInFile(DeviceInfo.coreList,
                    DeviceInfo.pathToInfoFile);

            // Write the general cores info to the CsV file

            String actualCmd = "CPU, Description, Latency, Name"
                    + System.getProperty("line.separator");
            cStatesPen.write(actualCmd);

            for (int cpu = 0; cpu < DeviceInfo.coreList.size(); cpu++) {
                BufferedReader infoReader = new BufferedReader(new FileReader
                        (new File(DeviceInfo.pathToInfoFile)));
                for (int state = 0; state < DeviceInfo.coreList.get(cpu); state++) {
                    actualCmd = "CPU" + cpu + ",";
                    int num = 0;
                    while (num < 3) {
                        String line = infoReader.readLine();
                        if (line.contains("POLL")) {
                            line = line.replace("POLL", "C0");
                        }

                        // System.out.println("pbbb1" + line + ", "
                        // + line.length());
                        actualCmd = actualCmd
                                + line.substring(0, line.length()).replace(",",
                                " and ");
                        actualCmd = actualCmd + ",";
                        num = num + 1;

                    }
                    actualCmd = actualCmd
                            + System.getProperty("line.separator");
                    cStatesPen.write(actualCmd);

                }
                infoReader.close();
            }

            cStatesPen.write(System.getProperty("line.separator")
                    + System.getProperty("line.separator"));

            Double newTotalTime = DeviceInfo.totalCstatesDuration / 1000;
            cStatesPen.write("Total test duration is " + newTotalTime
                    + " seconds" + System.getProperty("line.separator")
                    + System.getProperty("line.separator"));

            cStatesPen.write("Core, State, Duration In State, Total Transitions,Percentage in State, Number of Transitions per sec"
                    + System.getProperty("line.separator"));

            BufferedWriter cStatesValuePen = new BufferedWriter(new
                    FileWriter(new File(DeviceInfo.pathToCstatesValueFile)));

            String currentCmd = "";
            for (int i = 0; i < summaryCores.size(); i++) {
                for (int j = 0; j < summaryCores.get(i).size(); j++) {

                    if (summaryCores.get(i).get(j).equals("State0")) {
                        currentCmd = "C0,";
                    } else if (summaryCores.get(i).get(j).equals("State1")) {
                        currentCmd = "C1,";
                    } else if (summaryCores.get(i).get(j).equals("State2")) {
                        currentCmd = "C1E,";
                    } else if (summaryCores.get(i).get(j).equals("State3")) {
                        currentCmd = "C3,";
                    } else if (summaryCores.get(i).get(j).equals("State4")) {
                        currentCmd = "C6,";
                    } else if (summaryCores.get(i).get(j).equals("State5")) {
                        currentCmd = "C7,";
                    } else {
                        if (j == 3
                                && summaryCores.get(i).get(1).equals("State0")) {
                            currentCmd = "0,";
                        } else if (j == 5
                                && summaryCores.get(i).get(1).equals("State0")) {
                            currentCmd = "0,";
                        } else {
                            currentCmd = summaryCores.get(i).get(j) + ",";
                        }
                    }

                    cStatesPen.write(currentCmd);
                    cStatesValuePen.write(currentCmd);

                }
                cStatesPen.write(System.getProperty("line.separator"));
                cStatesValuePen.write(System.getProperty("line.separator"));
            }
            cStatesPen.close();
            cStatesValuePen.close();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private ArrayList<ArrayList<String>> getCstatesStats(ArrayList<Integer>
            coreList) {
        String[] cpuIdle = { "time", "usage" };
        ArrayList<ArrayList<String>> statesList = new
                ArrayList<ArrayList<String>>();

        for (int cpu = 0; cpu < coreList.size(); cpu++) {
            for (int state = 0; state < coreList.get(cpu); state++) {
                ArrayList<String> currentStats = new ArrayList<String>();
                for (String item : cpuIdle) {
                    String statesFile = "/sys/devices/system/cpu/cpu" + cpu
                            + "/cpuidle/state" + state + "/" + item;
                    // System.out.println("wwwwwwwwwww" + statesFile);
                    try {
                        BufferedReader statesReader = new BufferedReader(new
                                FileReader(new File(statesFile)));
                        String currrentValue = statesReader.readLine();
                        currentStats.add(currrentValue);
                        statesReader.close();
                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
                statesList.add(currentStats);
            }
        }
        return statesList;
    }
}
