package com.intel.amr.ssg.android.powermon.powertool.backend;

import android.os.Environment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.StringTokenizer;

public class DeviceInfo {
    public static Double totalCstatesDuration;
    public static String pathToInterruptsFile;
    public static String pathToSoftwareInterruptsFile;
    public static int globalInterruptError;
    public static int globalInterruptTime;
    public static int globalTime;
    public static int globalError;
    public static String ticklessOutputFilePath;
    public static int globalCstatesTime;
    public static int globalCstatesError;
    public static int globalAllTestsTime;
    public static int globalAllTestsError;
    public static String pathToCstatesFile;
    public static ArrayList<Integer> coreList;
    public static String pathToInfoFile;
    public static String pathToCstatesValueFile;
    public static String pathToTicklessInterruptsValuesFile;
    public static String interruptsValuesFile;
    public static String softwareInterruptsValuesFile;
    public static boolean isAllTests = false;
    public static boolean isDeferredTest;
    public static String pathToIoAnalysisFile;
    public static String pathToDevicePowerAnalysisFile;
    public static String pathToDevicePowerDumpFile;
    public static String pathToPchDataFile;
    public static int duration;

    public static ArrayList<Integer> checkCores() {
        File CpuFolder = new File("/sys/devices/system/cpu");
        ArrayList<File> CpuFiles = new
                ArrayList<File>(Arrays.asList(CpuFolder.listFiles()));
        coreList = new ArrayList<Integer>();
        int CoresCount = 0, StateCount = 0;
        Iterator CpuFolderIterator = CpuFiles.iterator();
        while (CpuFolderIterator.hasNext()) {
            String CurrentFileName = ((File) CpuFolderIterator.next())
                    .getName();
            // System.out.println("cdcccccc0" + ", " + ", " + CurrentFileName
            // + CurrentFileName.length());
            if (CurrentFileName.length() == 4
                    && CurrentFileName.startsWith("cpu")) {
                CoresCount++;
            }

        }

        for (int i = 0; i < CoresCount; i++) {
            StateCount = 0;
            String FolderName = "/sys/devices/system/cpu/cpu" + i + "/cpuidle";
            // System.out.println("efeeeeee" + FolderName);
            File StateFolder = new File(FolderName);
            ArrayList<File> StateFiles = new
                    ArrayList<File>(Arrays.asList(StateFolder.listFiles()));
            Iterator StateFolderIterator = StateFiles.iterator();

            while (StateFolderIterator.hasNext()) {
                String CurrentFileName = ((File) StateFolderIterator.next()).getName();
                if (CurrentFileName.startsWith("sta")) {
                    StateCount++;
                }
            }
            coreList.add(StateCount);
        }
        // System.out.println("cdcccccc1" + ", " + CoresCount + ", " +
        // coreList);
        return coreList;
    }

    public static void getCpuCoresInfoInFile(ArrayList<Integer> CoreList,
            String inputPathToInfoFile) {

        // System.out.println("cdcccccc" + CoreList);
        String[] Characterestics = { "desc", "latency", "name" };
        Environment.getExternalStorageDirectory();
        Environment.getExternalStorageState();

        try {
            BufferedWriter InfoPen = new BufferedWriter(new FileWriter(new
                    File(inputPathToInfoFile)));
            for (int cpu = 0; cpu < CoreList.size(); cpu++) {
                for (int states = 0; states < CoreList.get(cpu); states++) {
                    for (String Item : Characterestics) {
                        String FileName = "/sys/devices/system/cpu/cpu" + cpu
                                + "/cpuidle/state" + states + "/" + Item;
                        // System.out.println("abaaaaaaa" + FileName);
                        BufferedReader InfoReader = new BufferedReader(new FileReader(new File(FileName)));
                        String CurrentInfo = InfoReader.readLine();
                        InfoPen.write(CurrentInfo);
                        InfoPen.write(System.getProperty("line.separator"));
                        InfoReader.close();
                    }
                }
                InfoPen.close();
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void getGeneralCstatesInformation(ArrayList<Integer>
            coresList, String pathToInfoFile, String
            pathToGeneralInformationFile) {
        // System.out.println("gagagagag - " + coresList);
        try {
            BufferedWriter GeneralInformationPen = new BufferedWriter(new
                    FileWriter(new File(pathToGeneralInformationFile)));
            BufferedReader InfoReader;
            // = new BufferedReader(new FileReader(
            // new File(pathToInfoFile)));

            GeneralInformationPen.write("Cpu, Description, Latency, Name");
            GeneralInformationPen.write(System.getProperty("line.separator"));
            String toWrite = "";
            String contentFromInfo = "";

            for (int cpu = 0; cpu < coresList.size(); cpu++) {
                InfoReader = new BufferedReader(new FileReader(new
                        File(pathToInfoFile)));
                for (int states = 0; states < coresList.get(cpu); states++) {
                    toWrite = System.getProperty("line.separator") + "Cpu"
                            + cpu + ",";
                    GeneralInformationPen.write(toWrite);
                    int col = 0;

                    while (col < 3) {
                        contentFromInfo = InfoReader.readLine();
                        // System.out.println("xzxxxxxxx" + contentFromInfo);
                        if (contentFromInfo.contains("POLL")) {// && col == 2) {
                            toWrite = "C0,";
                        } else {
                            toWrite = contentFromInfo + ",";
                        }
                        // System.out.println("ababababab7 - " + toWrite);
                        GeneralInformationPen.write(toWrite);
                        col++;
                    }
                }
                InfoReader.close();
            }

            GeneralInformationPen.write(System.getProperty("line.separator"));
            GeneralInformationPen.write(System.getProperty("line.separator"));
            GeneralInformationPen.close();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public static void getGeneralPstatesInformation(String
            pathToGeneralInformationFile) {
        try {
            BufferedReader contentReader;
            // System.out.println("knkkkkk" + pathToGeneralInformationFile);
            contentReader = new BufferedReader(new FileReader(new
                    File("/sys/devices/system/cpu/cpu0/cpufreq/related_cpus")));
            int relatedCpus = Integer.parseInt(contentReader.readLine());
            contentReader.close();
            // System.out.println("knkkkkk10" + pathToGeneralInformationFile);
            contentReader = new BufferedReader(new FileReader(new
                    File("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq")));
            int maxFreq = Integer.parseInt(contentReader.readLine());
            contentReader.close();
            contentReader = new BufferedReader(new FileReader(new
                    File("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq")));
            int minFreq = Integer.parseInt(contentReader.readLine());
            contentReader.close();
            contentReader = new BufferedReader(new FileReader(new
                    File("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_transition_latency")));
            String cpuTransitionLatency = contentReader.readLine();
            contentReader.close();
            contentReader = new BufferedReader(new FileReader(new
                    File("/sys/devices/system/cpu/cpu0/cpufreq/scaling_driver")));
            String scalingDriver = contentReader.readLine();
            contentReader.close();
            contentReader = new BufferedReader(new FileReader(new
                    File("/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor")));
            String scalingGovernor = contentReader.readLine();
            contentReader.close();
            contentReader = new BufferedReader(new FileReader(new
                    File("/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors")));
            String availableGovernors = contentReader.readLine().replace(" ",
                    ",");
            contentReader.close();
            // System.out.println("knkkkkk11" + pathToGeneralInformationFile);
            contentReader = new BufferedReader(new FileReader(new
                    File("/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq")));
            int scalingMinFreq = Integer.parseInt(contentReader.readLine());
            contentReader.close();
            // System.out.println("knkkkkk12" + pathToGeneralInformationFile);
            // contentReader = new BufferedReader(new FileReader(new File(
            // "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq")));
            // System.out.println("knkkkkk123" + pathToGeneralInformationFile);

            // int scalingMaxFreq = Integer.parseInt(contentReader.readLine());
            // System.out.println("knkkkkk0" + pathToGeneralInformationFile);
            BufferedWriter PstatesInfoPen = new BufferedWriter(new
                    FileWriter(new File(pathToGeneralInformationFile), true));
            // System.out.println("knkkkkk1" + pathToGeneralInformationFile);
            PstatesInfoPen.write(System.getProperty("line.separator")
                    + System.getProperty("line.separator")
                    + "Performance Power States Info"
                    + System.getProperty("line.separator")
                    + System.getProperty("line.separator"));

            PstatesInfoPen.write(System.getProperty("line.separator")
                    + "Related Cpus," + relatedCpus);
            PstatesInfoPen.write(System.getProperty("line.separator")
                    + "Cpu Max freq," + maxFreq);
            PstatesInfoPen.write(System.getProperty("line.separator")
                    + "Cpu Min freq," + minFreq);
            PstatesInfoPen.write(System.getProperty("line.separator")
                    + "Cpu transition latency," + cpuTransitionLatency);
            PstatesInfoPen.write(System.getProperty("line.separator")
                    + "Scaling driver," + scalingDriver);
            PstatesInfoPen.write(System.getProperty("line.separator")
                    + "Scaling governor," + scalingGovernor);
            PstatesInfoPen.write(System.getProperty("line.separator")
                    + "Available governors," + availableGovernors);
            PstatesInfoPen.write(System.getProperty("line.separator")
                    + "Scaling Min freq," + scalingMinFreq);
            // PstatesInfoPen.write(System.getProperty("line.separator")
            // + ",Scaling Max freq," + scalingMaxFreq);
            PstatesInfoPen.close();
            contentReader.close();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void getAcpiWakeupInformation(String
            pathToGeneralInformationFile) {
        BufferedReader contentReader;
        String lineRead;
        ArrayList<ArrayList<String>> contentListToWrite = new
                ArrayList<ArrayList<String>>();
        // System.out.println("knkkkkk" + pathToGeneralInformationFile);
        try {
            contentReader = new BufferedReader(new FileReader(new
                      File("/proc/acpi/wakeup")));

            while ((lineRead = contentReader.readLine()) != null) {
                StringTokenizer st = new StringTokenizer(lineRead);
                ArrayList<String> line1 = new ArrayList<String>();
                while (st.hasMoreTokens()) {
                    String cs = st.nextToken();
                    line1.add(cs);
                }
                contentListToWrite.add(line1);
            }

            contentReader.close();

            BufferedWriter contentPen = new BufferedWriter(new FileWriter(new
                    File(pathToGeneralInformationFile), true));

            contentPen.write(System.getProperty("line.separator")
                    + System.getProperty("line.separator")
                    + System.getProperty("line.separator") + "ACPI states"
                    + System.getProperty("line.separator")
                    + System.getProperty("line.separator"));

            for (ArrayList<String> eachLine : contentListToWrite) {
                for (String eachToken : eachLine) {
                    contentPen.write(eachToken + ",");
                }
                contentPen.write(System.getProperty("line.separator"));
            }

            contentPen.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void getTicklessInterrupts(String pathToResultsFile,
            int duration, String TicklessInterruptsValuesFile,
            boolean isDeferred) {
        isDeferredTest = isDeferred;
        ticklessOutputFilePath = pathToResultsFile;
        pathToTicklessInterruptsValuesFile = TicklessInterruptsValuesFile;
        globalTime = 1;
        globalError = 0;
        // signals call
        globalTime = 0;
        // System.out.println("Reached getTicklessInterrupts()");
        TicklessIrqTask ticklessThread = new TicklessIrqTask();
        Thread thread1 = new Thread(ticklessThread);
        thread1.start();

        int i = 0;

        try {
            while (i != duration) {
                Thread.sleep(1000);
                i++;
            }
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        globalTime = 1;

        try {
            if (thread1.isAlive()) {
                thread1.join();
            }
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void getInterrupts(String pathToResultsFile, int duration,
            String interruptsValuesFilePath) {
        pathToInterruptsFile = pathToResultsFile;
        interruptsValuesFile = interruptsValuesFilePath;
        globalInterruptTime = 1;
        globalInterruptError = 0;
        // signals call
        globalInterruptTime = 0;
        // System.out.println("Reached getInterrupts()");
        IrqTask InterruptsThread = new IrqTask();
        Thread thread1 = new Thread(InterruptsThread);
        thread1.start();

        int i = 0;

        try {
            while (i != duration) {
                Thread.sleep(1000);
                i++;
            }
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        globalInterruptTime = 1;

        try {
            if (thread1.isAlive()) {
                thread1.join();
            }
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void getSoftwareInterrupts(String pathToResultsFile,
            int duration, String interruptsValuesFilePath) {
        pathToSoftwareInterruptsFile = pathToResultsFile;
        softwareInterruptsValuesFile = interruptsValuesFilePath;
        globalInterruptTime = 1;
        globalInterruptError = 0;
        // signals call
        globalInterruptTime = 0;
        // System.out.println("Reached getInterrupts()");
        SoftIrqTask softwareInterruptsThread = new SoftIrqTask();
        Thread thread1 = new Thread(softwareInterruptsThread);
        thread1.start();

        int i = 0;

        try {
            while (i != duration) {
                Thread.sleep(1000);
                i++;
            }
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        globalInterruptTime = 1;

        try {
            if (thread1.isAlive()) {
                thread1.join();
            }
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void getCstates(String pathToResultsFile, int duration,
            String inputPathToInfoFile, String cStatesValueFile) {
        pathToInfoFile = inputPathToInfoFile;
        pathToCstatesFile = pathToResultsFile;
        pathToCstatesValueFile = cStatesValueFile;
        totalCstatesDuration = (double) (duration * 1000);
        globalCstatesTime = 1;
        globalCstatesError = 0;
        // signals call
        globalCstatesTime = 0;
        coreList = checkCores();
        // System.out.println("Reached getCstates()");
        CstatesTask CstatesTask = new CstatesTask();
        Thread thread1 = new Thread(CstatesTask);
        thread1.start();

        int i = 0;

        try {
            while (i != duration) {
                Thread.sleep(1000);
                i++;
            }
            globalCstatesTime = 1;
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            if (thread1.isAlive()) {
                thread1.join();
            }
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
        }
    }

    public static void getAllTests(String pathToCstatesFile,
            String pathToInterruptsFile, String pathToTicklessInterruptsFile,
            int duration, String inputPathToInfoFile, String cStatesValueFile,
            String interruptsValueFile, String ticklessInterruptsValueFile) {
        DeviceInfo.pathToInfoFile = inputPathToInfoFile;
        DeviceInfo.pathToCstatesFile = pathToCstatesFile;
        DeviceInfo.ticklessOutputFilePath = pathToTicklessInterruptsFile;
        DeviceInfo.pathToInterruptsFile = pathToInterruptsFile;
        DeviceInfo.pathToCstatesValueFile = cStatesValueFile;
        DeviceInfo.pathToTicklessInterruptsValuesFile = ticklessInterruptsValueFile;
        DeviceInfo.interruptsValuesFile = interruptsValueFile;

        DeviceInfo.isAllTests = true;
        globalAllTestsTime = 1;
        globalAllTestsError = 0;
        // signals call
        globalAllTestsTime = 0;
        coreList = checkCores();
        // System.out.println("Reached getCstates()");

        CstatesTask CstatesTask = new CstatesTask();
        Thread cStatesRunner = new Thread(CstatesTask);
        cStatesRunner.start();

        IrqTask InterruptsThread = new IrqTask();
        Thread interruptsRunner = new Thread(InterruptsThread);
        interruptsRunner.start();

        TicklessIrqTask ticklessInterruptsThread = new TicklessIrqTask();
        Thread ticklessInterruptsRunner = new Thread(ticklessInterruptsThread);
        ticklessInterruptsRunner.start();

        int i = 0;
        try {
            while (i != duration) {
                Thread.sleep(1000);
                i++;
            }
            globalAllTestsTime = 1;
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            if (cStatesRunner.isAlive()) {
                cStatesRunner.join();
            }
            if (interruptsRunner.isAlive()) {
                interruptsRunner.join();
            }
            if (ticklessInterruptsRunner.isAlive()) {
                ticklessInterruptsRunner.join();
            }
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void getIoAnalysis(String pathToIoAnalysisFile, int duration) {
        DeviceInfo.pathToIoAnalysisFile = pathToIoAnalysisFile;

        globalInterruptTime = 1;
        globalInterruptError = 0;
        // signals call
        globalInterruptTime = 0;
        // System.out.println("Reached getInterrupts()");
        IoAnalysisTask ioAnalysisThread = new IoAnalysisTask();
        Thread thread1 = new Thread(ioAnalysisThread);
        thread1.start();

        int i = 0;

        try {
            while (i != duration) {
                Thread.sleep(1000);
                i++;
            }
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        globalInterruptTime = 1;

        try {
            if (thread1.isAlive()) {
                thread1.join();
            }
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void getDevicePowerAnalysis(String pathToIoAnalysisFile,
            String pathToDevicePowerDumpFile) {
        DeviceInfo.pathToDevicePowerAnalysisFile = pathToIoAnalysisFile;
        DeviceInfo.pathToDevicePowerDumpFile = pathToDevicePowerDumpFile;

        int duration = 0;
        globalInterruptTime = 1;
        globalInterruptError = 0;
        // signals call
        globalInterruptTime = 0;
        DevicePowerAnalysisTask
                devicePowerAnalysisTask = new DevicePowerAnalysisTask();
        Thread thread1 = new Thread(devicePowerAnalysisTask);
        thread1.start();

        int i = 0;

        try {
            while (i != duration) {
                Thread.sleep(1000);
                i++;
            }
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        globalInterruptTime = 1;

        try {
            if (thread1.isAlive()) {
                thread1.join();
            }
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void getPchData(String pathToPchDataFile, int duration) {
        DeviceInfo.pathToPchDataFile = pathToPchDataFile;
        globalInterruptTime = 1;
        globalInterruptError = 0;
        DeviceInfo.duration = duration;
        // signals call
        globalInterruptTime = 0;
        // System.out.println("Reached getInterrupts()");
        PchDataTask pchDataThread = new PchDataTask();
        Thread thread1 = new Thread(pchDataThread);
        thread1.start();

        int i = 0;

        try {
            while (i != duration) {
                Thread.sleep(1000);
                i++;
            }
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        globalInterruptTime = 1;

        try {
            if (thread1.isAlive()) {
                thread1.join();
            }
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
