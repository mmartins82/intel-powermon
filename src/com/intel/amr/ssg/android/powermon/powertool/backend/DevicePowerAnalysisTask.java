package com.intel.amr.ssg.android.powermon.powertool.backend;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

/**
 * User: Marcelo Martins <martins@cs.brown.edu>
 * Date: 1/3/14
 * Time: 3:52 PM
 */
public class DevicePowerAnalysisTask implements Runnable {
    HashMap<String, String> devicesMap = new HashMap<String, String>();

    public void run() {

        Process process;
        String cmd = "lspci -vv > " + DeviceInfo.pathToDevicePowerDumpFile;

        try {
            // process = Runtime.getRuntime().exec(new String[] { "su", "-c", cmd });
            // process.waitFor();
            //
            // int exitCode = process.exitValue();
            int exitCode = 0;
            if (exitCode == 0) {
                String lineRead;
                BufferedReader
                        dumpReader = new BufferedReader(new FileReader(new File(
                        DeviceInfo.pathToDevicePowerDumpFile)));

                while ((lineRead = dumpReader.readLine()) != null) {
                    if (lineRead.startsWith("00:")) {
                        String[] splits = lineRead.split(" ");
                        devicesMap.put(splits[0], lineRead.substring(8));
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        postProcessDevicePowerAnalysis();
    }

    private void postProcessDevicePowerAnalysis() {
        try {
            BufferedWriter devicePowerPen = new BufferedWriter(new FileWriter
                    (new File(DeviceInfo.pathToDevicePowerAnalysisFile)));

            devicePowerPen.write("Device ID        ,Device Name,Control        ,Runtime Status"
                    + System.getProperty("line.separator")
                    + System.getProperty("line.separator"));

            for (String currentDeviceId : devicesMap.keySet()) {
                String deviceName = devicesMap.get(currentDeviceId);
                String folderContainingFiles = "/sys/bus/pci/devices/0000:"
                        + currentDeviceId + "/power/";
                String control = "";
                String runtimeStatus = "";

                BufferedReader contentReader = new BufferedReader(new
                        FileReader(new File(folderContainingFiles
                        + "control")));
                control = contentReader.readLine();
                contentReader.close();
                contentReader = new BufferedReader(new FileReader(new
                        File(folderContainingFiles + "runtime_status")));
                runtimeStatus = contentReader.readLine();
                contentReader.close();
                devicePowerPen.write(currentDeviceId + "," + deviceName
                        + "        ," + control + "," + runtimeStatus
                        + System.getProperty("line.separator"));
            }

            devicePowerPen.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
