package com.intel.amr.ssg.android.powermon.powertool.backend;

import android.content.Context;
import com.intel.amr.ssg.android.powermon.powertool.utils.FtraceAccess;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.Callable;

/**
 * User: Marcelo Martins <martins@cs.brown.edu>
 * Date: 12/27/13
 * Time: 3:41 PM
 */
public class FtracePowerTask implements Callable<String[]> {
    private Context context;
    private String outpath;
    private String[] trace;
    private int duration;

    /**
     * Set context (necessary to parse kernel wakelocks)
     *
     * @param context
     */
    public void setContext(Context context) {
        this.context = context;
    }

    /**
     * Set filepath of computation output
     *
     * @param filepath output file path
     */
    public void setOutputFile(String filepath) {
        this.outpath = filepath;
    }

    /**
     * Set thread duration
     *
     * @param duration thread duration in s
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }

    /**
     * Enable tracing of kernel power events and return call graph
     *
     * @return trace output
     * @throws Exception
     */
    public String[] call() throws Exception {
        FtraceAccess.enableTracerEventCategory("power", true);
        boolean previouslyOn = FtraceAccess.isTracingOn();

        if (!previouslyOn) {
            FtraceAccess.enableTracing(true);
        }

        Thread.sleep(duration * 1000);

        // Restore tracing to off only if necessary
        if (!previouslyOn) {
            FtraceAccess.enableTracing(false);
        }

        trace = FtraceAccess.getTrace();
        postProcess();

        return trace;
    }

    void postProcess() {
        String separator = System.getProperty("line.separator");
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(new
                    File(outpath)));
            for (String line : trace) {
                out.write(line);
            }
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
