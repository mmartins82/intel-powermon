package com.intel.amr.ssg.android.powermon.powertool.backend;

import com.intel.amr.ssg.android.powermon.powertool.utils.GpuAccess;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.Callable;

/**
 * User: Marcelo Martins <martins@cs.brown.edu>
 * Date: 12/30/13
 * Time: 9:06 AM
 */
public class GpuResidencyTask implements Callable<String[]> {
    private String outpath;
    private String[] trace;
    private int duration;

    /**
     * Set filepath of residency output
     *
     * @param filepath output file path
     */
    public void setOutputFile(String filepath) {
        this.outpath = filepath;
    }

    /**
     * Set thread duration
     *
     * @param duration thread duration in s
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }

    /**
     * Calculate and output GPU RC6 residency states
     *
     * @return trace output
     * @throws java.lang.InterruptedException
     */
    public String[] call() throws java.lang.InterruptedException {
        long startRc6, startRc6p, startRc6pp;
        long rc6TotalTime = 1; // avoid division by zero
        long totalTime = duration * 1000;
        long rc0Residency, rc6Residency, rc6pResidency, rc6ppResidency;
        rc0Residency = totalTime; // in case rc6 is not enabled
        rc6Residency = rc6pResidency = rc6ppResidency = -1;

        try {
            startRc6 = Long.valueOf(GpuAccess.getRc6Residency());
            startRc6p = Long.valueOf(GpuAccess.getRc6pResidency());
            startRc6pp = Long.valueOf(GpuAccess.getRc6ppResidency());

            Thread.sleep(duration * 1000);

            rc6Residency = Long.valueOf(GpuAccess.getRc6Residency())
                    - startRc6;
            rc6pResidency = Long.valueOf(GpuAccess.getRc6pResidency())
                    - startRc6p;
            rc6ppResidency = Long.valueOf(GpuAccess.getRc6ppResidency())
                    - startRc6pp;
            rc6TotalTime = rc6Residency + rc6pResidency + rc6ppResidency;
            rc0Residency = duration - rc6TotalTime;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        trace = new String[5];

        trace[0] = "GPU STATE\tResidency (ms)\tRelative (%)\n";
        trace[1] = String.format("RC0\t%d\t%f\n", rc0Residency,
                calcRatio(rc0Residency, totalTime));
        trace[2] = "RC6\t" + ((rc6Residency == -1) ? "N/A\tN/A" :
                String.format("%d\t%f\n", rc6Residency,
                        calcRatio(rc6Residency, totalTime))) + "\n";
        trace[3] = "RC6P\t" + ((rc6pResidency == -1) ? "N/A\tN/A" :
                String.format("%d\t%f", rc6pResidency,
                        calcRatio(rc6pResidency, totalTime))) + "\n";
        trace[4] = "RC6PP\t" + ((rc6ppResidency == -1) ? "N/A\tN/A" :
                String.format("%d\t%f", rc6ppResidency,
                        calcRatio(rc6ppResidency, totalTime))) + "\n";
        postProcess();

        return trace;
    }

    private double calcRatio(long portion, long total) {
        // avoid division by zero
        if (total == 0) {
            return 0.0;
        }

        double d = (portion / total) * 100.0;

        /* cope with rouding errors due to measurement interval */
        if (d < 0.0) {
            d = 0.0;
        }

        if (d > 100.0)
            d = 100.0;

        return d;
    }

    void postProcess() {
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(new
                    File(outpath)));
            for (String line : trace) {
                out.write(line);
            }
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
