package com.intel.amr.ssg.android.powermon.powertool.backend;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

/**
 * User: Marcelo Martins <martins@cs.brown.edu>
 * Date: 1/3/14
 * Time: 4:01 PM
 */
public class IoAnalysisTask implements Runnable {
    public void run() {
        try {
            BufferedReader
                    ioReader = new BufferedReader(new FileReader(new File("/proc/diskstats")));
            String thisLine;
            HashMap<String, ArrayList<String>> deviceIoDetailsMapBeforeTest =
                    new HashMap<String, ArrayList<String>>();
            HashMap<String, ArrayList<String>> deviceIoDetailsMapAfterTest =
                    new HashMap<String, ArrayList<String>>();
            long startTime = System.currentTimeMillis();

            while ((thisLine = ioReader.readLine()) != null) {
                ArrayList<String> thisDevice = new ArrayList<String>();
                String deviceName;

                // String[] thisDeviceMetrics = thisLine.split(" ");

                StringTokenizer st = new StringTokenizer(thisLine);
                ArrayList<String> dummyListToStoreAllColumns = new
                        ArrayList<String>();
                while (st.hasMoreTokens()) {
                    String cs = st.nextToken();
                    dummyListToStoreAllColumns.add(cs);
                }
                System.out.println("iiiiiiiiiiiiiiiiiii"
                        + dummyListToStoreAllColumns);
                deviceName = dummyListToStoreAllColumns.get(2);
                thisDevice.add(dummyListToStoreAllColumns.get(3));
                thisDevice.add(dummyListToStoreAllColumns.get(6));
                thisDevice.add(dummyListToStoreAllColumns.get(7));
                thisDevice.add(dummyListToStoreAllColumns.get(10));

                deviceIoDetailsMapBeforeTest.put(deviceName, thisDevice);
            }
            System.out.println("beffffore - " + deviceIoDetailsMapBeforeTest);

            int theTime = 0;
            while (DeviceInfo.globalInterruptTime == 0
                    || (
                    DeviceInfo.isAllTests && DeviceInfo.globalAllTestsTime == 0)) {
                Thread.sleep(1000);
                theTime++;
            }

            if (DeviceInfo.globalInterruptError != 1
                    || (
                    DeviceInfo.isAllTests && DeviceInfo.globalAllTestsError != 1)) {

                ioReader = new BufferedReader(new FileReader(new
                        File("/proc/diskstats")));

                while ((thisLine = ioReader.readLine()) != null) {
                    ArrayList<String> thisDevice = new ArrayList<String>();
                    String deviceName;

                    // String[] thisDeviceMetrics = thisLine.split(" ");

                    StringTokenizer st = new StringTokenizer(thisLine);
                    ArrayList<String> dummyListToStoreAllColumns = new
                            ArrayList<String>();
                    while (st.hasMoreTokens()) {
                        String cs = st.nextToken();
                        dummyListToStoreAllColumns.add(cs);
                    }
                    System.out.println("iiiiiiiiiiiiiiiiiii"
                            + dummyListToStoreAllColumns);
                    deviceName = dummyListToStoreAllColumns.get(2);
                    thisDevice.add(dummyListToStoreAllColumns.get(3));
                    thisDevice.add(dummyListToStoreAllColumns.get(6));
                    thisDevice.add(dummyListToStoreAllColumns.get(7));
                    thisDevice.add(dummyListToStoreAllColumns.get(10));

                    deviceIoDetailsMapAfterTest.put(deviceName, thisDevice);
                }
                System.out.println("afffter - " + deviceIoDetailsMapAfterTest);
            }
            long endTime = System.currentTimeMillis();
            long duration = endTime - startTime;

            postProcessIoAnalysis(deviceIoDetailsMapBeforeTest,
                    deviceIoDetailsMapAfterTest, duration);

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void postProcessIoAnalysis(HashMap<String, ArrayList<String>>
            deviceIoDetailsMapBeforeTest, HashMap<String, ArrayList<String>>
            deviceIoDetailsMapAfterTest, long duration) {
        int durationInSeconds = (int) duration / 1000;
        HashMap<String, ArrayList<String>> differenceMap = new HashMap<String,
                ArrayList<String>>();

        for (String thisKey : deviceIoDetailsMapAfterTest.keySet()) {
            ArrayList<String> valuesBeforeTest =
                    deviceIoDetailsMapBeforeTest.get(thisKey);
            ArrayList<String> valuesAfterTest =
                    deviceIoDetailsMapAfterTest.get(thisKey);
            ArrayList<String> differenceValues = new ArrayList<String>();

            for (int i = 0; i < 4; i++) {
                Long difference = Long.parseLong(valuesAfterTest.get(i))
                        - Long.parseLong(valuesBeforeTest.get(i));
                if (i == 0 || i == 2) {
                    difference = difference / durationInSeconds;
                }
                differenceValues.add(difference.toString());
            }

            differenceMap.put(thisKey, differenceValues);
        }
        System.out.println("diffffff - " + differenceMap);

        try {
            BufferedWriter ioPen = new BufferedWriter(new FileWriter(new
                    File(DeviceInfo.pathToIoAnalysisFile)));
            ioPen.write("Disk I/O Analysis");
            ioPen.write(System.getProperty("line.separator")
                    + System.getProperty("line.separator")
                    + System.getProperty("line.separator"));

            ioPen.write("Device name,Reads per second,Writes per second,Time spent reading(ms),Time spent writing(ms)");
            ioPen.write(System.getProperty("line.separator"));
            int deviceCount = 0;

            for (String thisKey : differenceMap.keySet()) {
                ArrayList<String> thisRowList = differenceMap.get(thisKey);
                if (!(Integer.parseInt(thisRowList.get(0)) == 0
                        && Integer.parseInt(thisRowList.get(1)) == 0
                        && Integer.parseInt(thisRowList.get(2)) == 0
                        && Integer.parseInt(thisRowList.get(3)) == 0)) {
                    ioPen.write(thisKey + ",");
                    ioPen.write(thisRowList.get(0) + "," + thisRowList.get(1)
                            + "," + thisRowList.get(2) + ","
                            + thisRowList.get(3) + ",");
                    ioPen.write(System.getProperty("line.separator"));
                    deviceCount++;
                }
            }

            if (deviceCount == 0) {
                ioPen.write(System.getProperty("line.separator")
                        + System.getProperty("line.separator")
                        + "No Disk I/O happened during test period.");
            }

            ioPen.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
