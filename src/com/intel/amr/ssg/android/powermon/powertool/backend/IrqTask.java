package com.intel.amr.ssg.android.powermon.powertool.backend;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * User: Marcelo Martins <martins@cs.brown.edu>
 * Date: 1/3/14
 * Time: 3:57 PM
 */
public class IrqTask implements Runnable {
    public void run() {
        long startTime = System.currentTimeMillis();
        ArrayList<String> startInterrupts = new ArrayList<String>();

        try {
            BufferedReader
                    startInterruptsReader = new BufferedReader(new FileReader(new File("/proc/interrupts")));
            String thisLine;
            while ((thisLine = startInterruptsReader.readLine()) != null) {
                startInterrupts.add(thisLine);
            }

            int theTime = 0;
            while (DeviceInfo.globalInterruptTime == 0
                    || (
                    DeviceInfo.isAllTests && DeviceInfo.globalAllTestsTime == 0)) {
                Thread.sleep(1000);
                theTime++;
            }

            if (DeviceInfo.globalInterruptError != 1
                    || (
                    DeviceInfo.isAllTests && DeviceInfo.globalAllTestsError != 1)) {
                ArrayList<String> endInterrupts = new ArrayList<String>();
                BufferedReader endInterruptsReader = new BufferedReader(new
                        FileReader(new File("/proc/interrupts")));
                while ((thisLine = endInterruptsReader.readLine()) != null) {
                    endInterrupts.add(thisLine);
                }
                long endTime = System.currentTimeMillis();
                long duration = endTime - startTime;

                postProcessingInterrupts(startInterrupts, endInterrupts,
                        duration);
            }

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void postProcessingInterrupts(ArrayList<String> startInterrupts,
            ArrayList<String> endInterrupts, long duration) {
        try {
            BufferedWriter interruptsPen = new BufferedWriter(new FileWriter
                    (new File(DeviceInfo.pathToInterruptsFile)));
            interruptsPen.write("Interrupts test"
                    + System.getProperty("line.separator"));
            long durationInSeconds = duration / 1000;
            interruptsPen.write("Test duration is " + durationInSeconds
                    + " seconds " + System.getProperty("line.separator")
                    + System.getProperty("line.separator"));
            System.out.println("Bbbbbefore - " + startInterrupts);
            System.out.println("Aaaafter - " + endInterrupts);

            ArrayList<ArrayList<String>> startDifferenceList = new
                    ArrayList<ArrayList<String>>();
            ArrayList<ArrayList<String>> endDifferenceList = new
                    ArrayList<ArrayList<String>>();

            for (int i = 0; i < startInterrupts.size(); i++) {
                ArrayList<String> startLine1 = new ArrayList<String>();
                String currentLine = startInterrupts.get(i).replace("\\r\\n",
                        "\\n");
                currentLine = currentLine.replace(",", "");
                currentLine = currentLine.replace(":", "");
                StringTokenizer st = new StringTokenizer(currentLine);
                ArrayList<String> line1 = new ArrayList<String>();

                while (st.hasMoreTokens()) {
                    String cs = st.nextToken();
                    line1.add(cs);
                    startLine1.add(cs);
                }

                startDifferenceList.add(startLine1);
            }

            for (int i = 0; i < endInterrupts.size(); i++) {
                ArrayList<String> endLine1 = new ArrayList<String>();
                String currentLine = endInterrupts.get(i).replace("\\r\\n",
                        "\\n");
                currentLine = currentLine.replace(",", "");
                currentLine = currentLine.replace(":", "");
                StringTokenizer st = new StringTokenizer(currentLine);
                ArrayList<String> line1 = new ArrayList<String>();

                while (st.hasMoreTokens()) {
                    String cs = st.nextToken();
                    line1.add(cs);
                    endLine1.add(cs);
                }

                endDifferenceList.add(endLine1);
            }
            interruptsPen.write("Interrupts during the test"
                    + System.getProperty("line.separator") + "Interrupt ID,");

            ArrayList<ArrayList<String>> differenceList = new
                    ArrayList<ArrayList<String>>();
            BufferedWriter interruptsValuesPen = new BufferedWriter(new
                    FileWriter(new File(DeviceInfo.interruptsValuesFile)));
            for (int i = 0; i < startDifferenceList.size(); i++) {
                ArrayList<String> differenceLine1 = new ArrayList<String>();
                for (int j = 0; j < startDifferenceList.get(i).size(); j++) {
                    differenceLine1.add(endDifferenceList.get(i).get(j));
                }
                differenceList.add(differenceLine1);
            }

            for (int i = 0; i < startDifferenceList.size(); i++) {
                for (int j = 0; j < startDifferenceList.get(i).size(); j++) {
                    if ((i > 0 && i < (startDifferenceList.size() - 2))
                            && (j >= 1 && j <= 4)) {
                        Integer differenceValue =
                                Integer.parseInt(endDifferenceList.get(i).get(j))
                                        - Integer.parseInt(startDifferenceList.get(i).get(j));
                        differenceList.get(i).set(j,
                                differenceValue.toString());
                    }
                }
            }

            for (int i = 0; i < differenceList.size(); i++) {
                for (int j = 0; j < differenceList.get(i).size(); j++) {
                    if (j == differenceList.get(i).size() - 1) {
                        if (i == 0) {
                            interruptsPen.write("CPU3,Interrupt type,Device Name"
                                    + System.getProperty("line.separator"));
                            interruptsValuesPen.write("CPU3,Interrupt type,Device Name"
                                    + System.getProperty("line.separator"));

                        } else {
                            if (differenceList.get(i).get(j).equals("i915")) {
                                interruptsPen.write("i915 DRM (Direct Rendering Manager)");
                            } else if (differenceList.get(i).get(j).equals("i8042")) {
                                interruptsPen.write("i8042 - Keyboard Controller");
                            } else if (differenceList.get(i).get(j).equals("mmc0")) {
                                interruptsPen.write("mmc0 - Multimedia Card");
                            } else if (differenceList.get(i).get(j).equals("INT33C300")) {
                                interruptsPen.write("I2C Touch Controller");
                            } else if (differenceList.get(i).get(j).equals("rtc0")) {
                                interruptsPen.write("rtc0 - Real Time Clock");
                            } else if (differenceList.get(i).get(j).equals("acpi")) {
                                interruptsPen.write("acpi - Advanced Configuration and Power Interface");
                            } else if (differenceList.get(i).get(j).equals("INT33D100")) {
                                interruptsPen.write("INT33D100 - Mouse light pen emulation");
                            } else if (differenceList.get(i).get(j).equals("dmar1")) {
                                interruptsPen.write("DMAR1 - Block interrupt");
                            } else if (differenceList.get(i).get(j).equals("ahci")) {
                                interruptsPen.write("ahci - Advanced Host Controller Interface");
                            } else if (differenceList.get(i).get(j).equals("xhci_hcd")) {
                                interruptsPen.write("xhci_hcd - Extensible Host Controller Interface");
                            } else if (differenceList.get(i).get(j).equals("snd_hda_intel")) {
                                interruptsPen.write("snd_hda_intel - HD Audio Codec");
                            } else {
                                interruptsPen.write(differenceList.get(i).get(j));
                            }
                            interruptsPen.write(System.getProperty("line.separator"));
                            interruptsValuesPen.write(System.getProperty("line.separator"));
                        }
                    } else {
                        interruptsPen.write(differenceList.get(i).get(j));
                        interruptsPen.write(",");
                        interruptsValuesPen.write(differenceList.get(i).get(j));
                        interruptsValuesPen.write(",");
                    }
                }

            }

            interruptsPen.close();
            interruptsValuesPen.close();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
