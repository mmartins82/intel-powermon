package com.intel.amr.ssg.android.powermon.powertool.backend;

import android.content.Context;
import com.asksven.android.common.kernelutils.NativeKernelWakelock;
import com.asksven.android.common.kernelutils.Wakelocks;
import com.asksven.android.common.privateapiproxies.StatElement;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * User: Marcelo Martins <martins@cs.brown.edu>
 * Date: 12/27/13
 * Time: 10:59 AM
 */
public class KernelWakelockTask implements
    Callable<List<NativeKernelWakelock>> {
    private Context context;
    private String outpath;
    private List<NativeKernelWakelock> kernelWakelocks;

    /**
     * Set context (necessary to parse kernel wakelocks)
     *
     * @param context
     */
    public void setContext(Context context) {
        this.context = context;
    }

    /**
     * Set filepath of computation output
     *
     * @param filepath output file path
     */
    public void setOutputFile(String filepath) {
        this.outpath = filepath;
    }

    /**
     * Parse kernel wakelocks from /proc, save to file and expose them as list
     *
     * @return list of kernel wakelocks
     * @throws Exception
     */
    public List<NativeKernelWakelock> call() throws Exception {
        List<StatElement> stats = Wakelocks.parseProcWakelocks(context);
        kernelWakelocks = new ArrayList<NativeKernelWakelock>();

        for (StatElement stat : stats) {
            kernelWakelocks.add((NativeKernelWakelock) stat);
        }

        postProcess();
        return kernelWakelocks;
    }

    void postProcess() {
        String separator = System.getProperty("line.separator");
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(new
                    File(outpath)));
            out.write("Kernel Wakelocks" + separator + separator + separator);
            out.write("UID\tWakelock Name\tWakelock count\tWakelock duration"
                    + " (us)" + separator);

            for (NativeKernelWakelock wl : kernelWakelocks) {
                String res = String.format("%d\t%s\t%d\t%d" + separator,
                        wl.getuid(), wl.getName(), wl.getCount(),
                        wl.getDuration());
                out.write(res);
            }

            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
