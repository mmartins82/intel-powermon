package com.intel.amr.ssg.android.powermon.powertool.backend;

import java.math.BigInteger;

class IOCTL_PMC_COUNTERS_OUT {
    BigInteger fg_mphy_pr;
    BigInteger fg_mphy_pr1;
    BigInteger fg_mphy_pg;
    BigInteger fg_pr_cntr_usb2afe;
    BigInteger fg_pr_cntr_pxp;
    BigInteger fg_pr_cntr_sata;
    BigInteger fg_pr_cntr_hda;
    BigInteger fg_pr_cntr_usb2;
    BigInteger fg_pr_cntr_opi;
    BigInteger fg_pr_cntr_icc;
    BigInteger fg_pr_cntr_adsp_hf;
    BigInteger fg_pr_cntr_adsp_lf;
}

class IOCTL_PMC_ACTIVITY {
    double fg_mphy_pr;
    double fg_mphy_pr1;
    double fg_mphy_pg;
    double fg_pr_cntr_usb2afe;
    double fg_pr_cntr_pxp;
    double fg_pr_cntr_sata;
    double fg_pr_cntr_hda;
    double fg_pr_cntr_usb2;
    double fg_pr_cntr_opi;
    double fg_pr_cntr_icc;
    double fg_pr_cntr_adsp_hf;
    double fg_pr_cntr_adsp_lf;
}

class ValuesAndVariables {
    enum fields {
        fg_mphy_pr, fg_mphy_pr1, fg_mphy_pg, fg_pr_cntr_usb2afe,
        fg_pr_cntr_pxp, fg_pr_cntr_sata, fg_pr_cntr_hda, fg_pr_cntr_usb2,
        fg_pr_cntr_opi, fg_pr_cntr_icc, fg_pr_cntr_adsp_hf, fg_pr_cntr_adsp_lf;
    }


    //MPHY Look up table for calculating dynamic power
    /**************************************************************************
    * mW*                Temperature (deg C)
    * Process Fuses     0-15 16-30 31-45 46-60 61-75 76-90 91-105 106 or higher
    * 00                8      10    14    19   26     36   50     56
    * 01                13      17    23      32   45      62   86      96
    * 10                16      23    32      45   63      87   118      130
    * 11                32      46    67    94   132    178  238    255
    **************************************************************************/

    long[] FUSE_00 = { 8, 10, 14, 19, 26, 36, 50, 56 };
    long[] FUSE_01 = { 13, 17, 23, 32, 45, 62, 86, 96 };
    long[] FUSE_10 = { 16, 23, 32, 45, 63, 87, 118, 130 };
    long[] FUSE_11 = { 32, 46, 67, 94, 132, 178, 238, 255 };
}
