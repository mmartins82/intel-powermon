package com.intel.amr.ssg.android.powermon.powertool.backend;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Time;
import java.util.Date;

/**
 * User: Marcelo Martins <martins@cs.brown.edu>
 * Date: 1/3/14
 * Time: 3:58 PM
 */
public class PchDataTask implements Runnable {
    public void run() {
        try {
            // The following parameters are used in the IOCTL call
            IOCTL_PMC_COUNTERS_OUT pmc_counter = new IOCTL_PMC_COUNTERS_OUT();
            IOCTL_PMC_COUNTERS_OUT pmc_counter_prev = new IOCTL_PMC_COUNTERS_OUT();

            IOCTL_PMC_ACTIVITY pmc_counter_percent = new IOCTL_PMC_ACTIVITY();
            IOCTL_PMC_ACTIVITY pmc_counter_power = new IOCTL_PMC_ACTIVITY();

            Time start_t, end_t, poll_start_t, poll_end_t; // timer for polling and runtime

            double diff_t, diff_t1; //timer for polling
            BufferedWriter pchPen; // output to csv file

            Date date_time; //to get date and timestamp
            String buffer = ""; // to store date and time
            String buffer1 = "";

            long count = 0;
            double poll_time = 1.0;
            long run_time = 0; //default to 1 second if no user input
            long temperature = 0;
            long fuse = 0;

            run_time = (long) DeviceInfo.duration;
            pchPen = new BufferedWriter(new FileWriter(new File(DeviceInfo.pathToPchDataFile)));

            pchPen.write("Date,Timestamp,cntr_0b,cntr_1b,cntr_pg,cntr_usb2afe,cntr_pxp,cntr_sata,cntr_hda,cntr_usb2,cntr_opi,cntr_icc,cntr_adsp_hf,cntr_adsp_if,");
            pchPen.write("mphy_lane1to8,mphy_lane9tp14,mphy,usb2_afe_x8,pcie_pll,sata_pll,audio_pll,usb2_pll,opi_pll,adsp_hf,adsp_lf,");
            pchPen.write("Temperature,Fuse,");
            pchPen.write("mphy_lane1to8Pwr,mphy_lane9to14Pwr,mphyPWr,usb2_afe_x8Pwr,pcie_pllPwr,sata_pllPwr,audio_pllPwr,usb2_pllPwr,opi_pllPwr,icc_pllPwr,adsp_hfPwr,adsp_lfPwr,");
            pchPen.write(System.getProperty("line.separator"));
            pchPen.write(System.getProperty("line.separator"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
