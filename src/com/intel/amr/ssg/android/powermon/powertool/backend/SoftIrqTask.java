package com.intel.amr.ssg.android.powermon.powertool.backend;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * User: Marcelo Martins <martins@cs.brown.edu>
 * Date: 1/3/14
 * Time: 3:59 PM
 */
public class SoftIrqTask implements Runnable {
    public void run() {
        long startTime = System.currentTimeMillis();
        ArrayList<String> startInterrupts = new ArrayList<String>();
        try {
            BufferedReader
                    startInterruptsReader = new BufferedReader(new FileReader(new File("/proc/softirqs")));
            String thisLIne;
            while ((thisLIne = startInterruptsReader.readLine()) != null) {
                startInterrupts.add(thisLIne);
            }

            int theTime = 0;
            while (DeviceInfo.globalInterruptTime == 0
                    || (
                    DeviceInfo.isAllTests && DeviceInfo.globalAllTestsTime == 0)) {
                Thread.sleep(1000);
                theTime++;
            }

            if (DeviceInfo.globalInterruptError != 1
                    || (
                    DeviceInfo.isAllTests && DeviceInfo.globalAllTestsError != 1)) {
                ArrayList<String> endInterrupts = new ArrayList<String>();
                BufferedReader endInterruptsReader = new BufferedReader(new
                        FileReader(new File("/proc/softirqs")));
                while ((thisLIne = endInterruptsReader.readLine()) != null) {
                    endInterrupts.add(thisLIne);
                }
                long endTime = System.currentTimeMillis();
                long duration = endTime - startTime;

                postProcessingSoftwareInterrupts(startInterrupts,
                        endInterrupts, duration);

            }

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void postProcessingSoftwareInterrupts(ArrayList<String>
            startInterrupts, ArrayList<String> endInterrupts,
            long duration) {
        try {
            BufferedWriter
                    softInterruptsPen = new BufferedWriter(new FileWriter(new File(
                    DeviceInfo.pathToSoftwareInterruptsFile)));
            softInterruptsPen.write("Software interrupts"
                    + System.getProperty("line.separator"));
            long durationInSeconds = duration / 1000;
            softInterruptsPen.write("Test duration is " + durationInSeconds
                    + " seconds " + System.getProperty("line.separator")
                    + System.getProperty("line.separator"));
            System.out.println("Bbbbbefore - " + startInterrupts);
            System.out.println("Aaaafter - " + endInterrupts);

            ArrayList<ArrayList<String>> startDifferenceList = new
                    ArrayList<ArrayList<String>>();
            ArrayList<ArrayList<String>> endDifferenceList = new
                    ArrayList<ArrayList<String>>();

            for (int i = 0; i < startInterrupts.size(); i++) {
                ArrayList<String> startLine1 = new ArrayList<String>();
                String currentLine = startInterrupts.get(i).replace("\\r\\n",
                        "\\n");
                currentLine = currentLine.replace(",", "");
                currentLine = currentLine.replace(":", "");
                StringTokenizer st = new StringTokenizer(currentLine);
                ArrayList<String> line1 = new ArrayList<String>();
                while (st.hasMoreTokens()) {
                    String cs = st.nextToken();
                    line1.add(cs);
                    startLine1.add(cs);
                }
                startDifferenceList.add(startLine1);
            }

            for (int i = 0; i < endInterrupts.size(); i++) {
                ArrayList<String> endLine1 = new ArrayList<String>();
                String currentLine = endInterrupts.get(i).replace("\\r\\n",
                        "\\n");
                currentLine = currentLine.replace(",", "");
                currentLine = currentLine.replace(":", "");
                StringTokenizer st = new StringTokenizer(currentLine);
                ArrayList<String> line1 = new ArrayList<String>();
                while (st.hasMoreTokens()) {
                    String cs = st.nextToken();
                    line1.add(cs);
                    endLine1.add(cs);
                }

                endDifferenceList.add(endLine1);
            }

            softInterruptsPen.write("Software interrupts during the test"
                    + System.getProperty("line.separator") + "Caused by,");

            ArrayList<ArrayList<String>> differenceList = new
                    ArrayList<ArrayList<String>>();
            BufferedWriter softInterruptsValuesPen = new BufferedWriter(new
                    FileWriter(new File(DeviceInfo.softwareInterruptsValuesFile)));
            for (int i = 0; i < startDifferenceList.size(); i++) {
                ArrayList<String> differenceLine1 = new ArrayList<String>();
                if (i == 0) {
                    for (int j = 0; j < 4; j++) {
                        differenceLine1.add(endDifferenceList.get(i).get(j));
                    }
                } else {
                    for (int j = 0; j < 5; j++) {
                        differenceLine1.add(endDifferenceList.get(i).get(j));
                    }
                }

                differenceList.add(differenceLine1);
            }

            for (int i = 0; i < startDifferenceList.size(); i++) {

                for (int j = 0; j < 5; j++) {
                    if ((i > 0) && (j >= 1)) {
                        Integer differenceValue =
                                Integer.parseInt(endDifferenceList.get(i).get(j))
                                        - Integer.parseInt(startDifferenceList.get(i).get(j));
                        differenceList.get(i).set(j, differenceValue.toString());
                    }
                }
            }

            for (int i = 0; i < differenceList.size(); i++) {
                for (int j = 0; j < 5; j++) {
                    if (i == 0 && j == 4) {
                        softInterruptsPen.write("Total interrupts per second (all cores),"
                                + System.getProperty("line.separator"));
                        softInterruptsValuesPen.write("Total interrupts per second (all cores),"
                                + System.getProperty("line.separator"));
                    } else {
                        if (j == 4) {

                            int total =
                                    Integer.parseInt(differenceList.get(i).get(1))
                                            + Integer.parseInt(differenceList.get(i).get(2))
                                            + Integer.parseInt(differenceList.get(i).get(3))
                                            + Integer.parseInt(differenceList.get(i).get(4));
                            double interruptsPerSecond = (double) total
                                    / (double) durationInSeconds;

                            softInterruptsPen.write(differenceList.get(i).get(j)
                                    + "," + interruptsPerSecond);
                            softInterruptsPen.write(System.getProperty("line.separator"));
                            softInterruptsValuesPen.write(differenceList.get(i).get(j)
                                    + "," + interruptsPerSecond);
                            softInterruptsValuesPen.write(System.getProperty("line.separator"));
                        } else {
                            softInterruptsPen.write(differenceList.get(i).get(j));
                            softInterruptsPen.write(",");
                            softInterruptsValuesPen.write(differenceList.get(i).get(j));
                            softInterruptsValuesPen.write(",");
                        }
                    }
                }
            }
            softInterruptsPen.close();
            softInterruptsValuesPen.close();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
