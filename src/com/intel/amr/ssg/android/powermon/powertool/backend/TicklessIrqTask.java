package com.intel.amr.ssg.android.powermon.powertool.backend;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * User: Marcelo Martins <martins@cs.brown.edu>
 * Date: 1/3/14
 * Time: 4:00 PM
 */
public class TicklessIrqTask implements Runnable {
    public void run() {
        try {
            // System.out.println("namma thread started");

            Process process;
            String cmd = "echo 1 > /proc/timer_stats";
            process = Runtime.getRuntime().exec(new String[]{"su", "-c", cmd});
            process.waitFor();
            int exitCode = process.exitValue();
            String almessage = "";

            if (exitCode == 0) {
                almessage = "1 Wwwwwrite success";
            } else {
                almessage = "1 Wwwwwrite failed";
            }

            int theTime = 0;
            // System.out.println("ggglobaltime = " + DeviceInfo.globalTime);
            while (DeviceInfo.globalTime == 0
                    || (
                    DeviceInfo.isAllTests && DeviceInfo.globalAllTestsTime == 0)) {
                Thread.sleep(1000);
                theTime++;
            }

            if (DeviceInfo.globalError != 1
                    || (
                    DeviceInfo.isAllTests && DeviceInfo.globalAllTestsError != 1)) {
                // System.out.println("ggglobalerror != 1 loop");
                cmd = "echo 0 > /proc/timer_stats";
                process = Runtime.getRuntime().exec(new String[] { "su", "-c",
                        cmd });
                process.waitFor();
                exitCode = process.exitValue();
                almessage = "";

                if (exitCode == 0) {
                    almessage = "0 Wwwwwrite success";
                } else {
                    almessage = "0 Wwwwwrite failed";
                }
                // System.out.println(almessage);

                // BufferedWriter timerStatsWriter1 = new BufferedWriter(
                // new FileWriter(new File("/proc/timer_stats")));
                // timerStatsWriter1.write("0");
                // timerStatsWriter1.close();
                if (DeviceInfo.isDeferredTest) {
                    postProcessingDeferred();
                } else {
                    postProcessingTickless();
                }
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void postProcessingTickless() {
        ArrayList<String> normalTicklessInterrupts = new ArrayList<String>();
        ArrayList<String> deferredTicklessInterrupts = new ArrayList<String>();
        String lineRead = "";
        try {
            // System.out.println("kskkkkkkkkk" + DeviceInfo.ticklessOutputFilePath);
            BufferedWriter
                    ticklessResultsPen = new BufferedWriter(new FileWriter(new File(
                    DeviceInfo.ticklessOutputFilePath)));
            BufferedReader timerStatsReader = new BufferedReader(new FileReader(new File("/proc/timer_stats")));
            while ((lineRead = timerStatsReader.readLine()) != null) {
                if (lineRead.contains("D,")) {
                    deferredTicklessInterrupts.add(lineRead);
                } else {
                    normalTicklessInterrupts.add(lineRead);
                }
            }

            for (int i = 0; i < normalTicklessInterrupts.size(); i++) {
                normalTicklessInterrupts.get(i).replace("\\r\\n", "\\n");
                normalTicklessInterrupts.get(i).replace(",", "");
                if (i < 2) {
                    ticklessResultsPen.write(normalTicklessInterrupts.get(i));
                } else if (i == 2) {
                    ticklessResultsPen.write(System.getProperty("line.separator")
                            + "Number of events, Process ID, Process name, Initialized by, Function executed on expiry"
                            + System.getProperty("line.separator"));
                    StringTokenizer st = new
                            StringTokenizer(normalTicklessInterrupts.get(i));
                    ArrayList<String> line2 = new ArrayList<String>();

                    while (st.hasMoreTokens()) {
                        line2.add(st.nextToken());
                    }

                    for (int j = 0; j < line2.size(); j++) {
                        if (j == 0) {
                            ticklessResultsPen.write(line2.get(j));
                            // ticklessResultsPen.write(",");
                        } else if (j == line2.size() - 1) {
                            ticklessResultsPen.write(line2.get(j));
                            ticklessResultsPen.write(System
                                    .getProperty("line.separator"));
                        } else {
                            ticklessResultsPen.write(line2.get(j));
                            ticklessResultsPen.write(",");
                        }
                    }
                } else {
                    StringTokenizer st = new
                            StringTokenizer(normalTicklessInterrupts.get(i));
                    ArrayList<String> line2 = new ArrayList<String>();
                    while (st.hasMoreTokens()) {
                        line2.add(st.nextToken());
                    }

                    for (int j = 0; j < line2.size(); j++) {
                        if (j == 0) {
                            ticklessResultsPen.write(line2.get(j));
                            // ticklessResultsPen.write(",");
                        } else if (j == line2.size() - 1) {
                            ticklessResultsPen.write(line2.get(j));
                            ticklessResultsPen.write(System.getProperty("line.separator"));
                        } else {
                            ticklessResultsPen.write(line2.get(j));
                            ticklessResultsPen.write(",");
                        }
                    }
                }
            }
            timerStatsReader.close();
            ticklessResultsPen.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void postProcessingDeferred() {
        ArrayList<String> normalTicklessInterrupts = new ArrayList<String>();
        ArrayList<String> deferredTicklessInterrupts = new ArrayList<String>();
        String lineRead = "";
        try {
            // System.out.println("kskkkkkkkkk" + DeviceInfo.ticklessOutputFilePath);
            BufferedWriter deferredResultsPen = new BufferedWriter(new
                    FileWriter(new File(DeviceInfo.ticklessOutputFilePath)));
            BufferedReader timerStatsReader = new BufferedReader(new
                    FileReader(new File("/proc/timer_stats")));
            while ((lineRead = timerStatsReader.readLine()) != null) {
                if (lineRead.contains("D,")) {
                    deferredTicklessInterrupts.add(lineRead);
                } else {
                    normalTicklessInterrupts.add(lineRead);
                }
            }

            for (int i = 0; i < deferredTicklessInterrupts.size(); i++) {
                deferredTicklessInterrupts.get(i).replace("\\r\\n", "\\n");
                deferredTicklessInterrupts.get(i).replace("D,", "");
                deferredTicklessInterrupts.get(i).replace(",", "");

                if (i < 0) {

                    deferredResultsPen.write(deferredTicklessInterrupts.get(i));
                } else if (i == 0) {
                    deferredResultsPen.write(System.getProperty("line.separator")
                            + "Number of events, Process ID, Process name, Initialized by, Function executed on expiry"
                            + System.getProperty("line.separator"));
                    StringTokenizer st = new
                            StringTokenizer(deferredTicklessInterrupts.get(i));
                    ArrayList<String> line2 = new ArrayList<String>();
                    while (st.hasMoreTokens()) {
                        line2.add(st.nextToken());
                    }

                    for (int j = 0; j < line2.size(); j++) {
                        if (j == 0) {
                            deferredResultsPen.write(line2.get(j).replace("D",
                                    ""));
                        } else if (j == line2.size() - 1) {
                            deferredResultsPen.write(line2.get(j));
                            deferredResultsPen.write(System.getProperty("line.separator"));
                        } else {
                            deferredResultsPen.write(line2.get(j));
                            deferredResultsPen.write(",");
                        }
                    }
                } else {
                    StringTokenizer st = new
                            StringTokenizer(deferredTicklessInterrupts.get(i));
                    ArrayList<String> line2 = new ArrayList<String>();
                    while (st.hasMoreTokens()) {
                        line2.add(st.nextToken());
                    }

                    for (int j = 0; j < line2.size(); j++) {
                        if (j == 0) {
                            deferredResultsPen.write(line2.get(j).replace("D",
                                    ""));
                        } else if (j == line2.size() - 1) {
                            deferredResultsPen.write(line2.get(j));
                            deferredResultsPen.write(System.getProperty("line.separator"));
                        } else {
                            deferredResultsPen.write(line2.get(j));
                            deferredResultsPen.write(",");
                        }
                    }
                }
            }

            timerStatsReader.close();
            deferredResultsPen.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
