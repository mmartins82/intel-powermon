package com.intel.amr.ssg.android.powermon.powertool.backend;

import android.content.Context;
import com.intel.amr.ssg.android.powermon.powertool.utils.RootShell;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: Marcelo Martins <martins@cs.brown.edu>
 * Date: 12/24/13
 * Time: 3:21 PM
 */
public class UserWakelockTask implements Callable<List<String>> {

    private Context context;
    private String outpath;
    private List<String> userWakelocks;

    /**
     * Set filepath of computation output
     *
     * @param filename output file path
     */
    public void setOutputFile(String filename) {
        this.outpath = filename;
    }

    /**
     * Grab and return list of current user wakelocks with associated info
     *
     * @return list of user wakelocks
     * @throws Exception
     */
    public List<String> call() throws Exception {
        userWakelocks = getUserWakelocks();
        postProcess();
        return userWakelocks;
    }

    private List<String> getUserWakelocks() {
        RootShell shell = RootShell.getInstance();
        List<String> res = shell.run("dumpsys power");

        Iterator<String> iterator = res.iterator();
        int i = 0;


        // Find start of user wakelock declaration
        while (iterator.hasNext() ) {
            if (iterator.next().startsWith("Wake Locks")) {
                break;
            }

            i++;
        }

        int j = i;
        // Find end of user wakelock declaration
        while (iterator.hasNext()) {
            if (iterator.next().trim().equals("")) {
                break;
            }

            j++;
        }

        assert (j >= i);
        assert (j <= res.size());

        List<String> sublist = res.subList(i, j);
        String pt = "\\s+(\\w+)\\s+(.+)\\s+\\(uid=(\\d+), pid=(\\d+).*";
        Pattern pattern = Pattern.compile(pt);

        userWakelocks = new ArrayList<String>();
        userWakelocks.add("Lock_type\t\tApp_name\t\tUID\tPID");
        for (String wl : sublist) {
            Matcher m = pattern.matcher(wl);

            if (m.matches()) {
                String s = String.format("%s\t%s\t%s\t%s", m.group(1),
                        m.group(2), m.group(3), m.group(4));
                userWakelocks.add(s);
            }
        }

        return userWakelocks;
    }

    /*
     * Save user wakelocks to file
     */
    void postProcess() {
        String separator = System.getProperty("line.separator");
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(new
                    File(outpath)));

            for (String wl : userWakelocks) {
                out.write(wl + "\n");
            }

            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
