package com.intel.amr.ssg.android.powermon.powertool.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.intel.amr.ssg.android.powermon.R;
import com.intel.amr.ssg.android.powermon.powertool.backend.DeviceInfo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class CstatesActivity extends Activity {
    ProgressDialog dialog;
    Button graphButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.general_information);

        final CStatesCollector collector = new CStatesCollector();
        collector.execute("none");

        graphButton = new Button(CstatesActivity.this);
        graphButton.setText("View graph");

        graphButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent GraphScreen = new Intent(CstatesActivity.this,
                        CstatesGraph.class);

                GraphScreen.putExtra("testDuration", getIntent().getExtras()
                        .getInt("testDuration"));

                ArrayList<String> Col11 = new ArrayList<String>();
                ArrayList<String> Col21 = new ArrayList<String>();
                ArrayList<String> Col31 = new ArrayList<String>();

                for (ArrayList<String> s : collector.dataFromFile) {
                    Col11.add(s.get(0));
                    Col21.add(s.get(4));
                    Col31.add(s.get(1));
                }

                GraphScreen.putStringArrayListExtra("Col1", Col11);
                GraphScreen.putStringArrayListExtra("Col2", Col21);
                GraphScreen.putStringArrayListExtra("Col3", Col31);
                startActivity(GraphScreen);
            }
        });
    }

    public class CStatesCollector extends AsyncTask<String, Integer, String> {

        public ArrayList<ArrayList<String>> dataFromFile = new
                ArrayList<ArrayList<String>>();

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(CstatesActivity.this);
            dialog.setIndeterminate(true);
            dialog.setCancelable(false);
            dialog.setMessage("Please wait for the results.....");
            dialog.show();
        }

        String cStatesFile;
        String cStatesValuesFile;

        @Override
        protected String doInBackground(String... params) {
            Bundle extras = getIntent().getExtras();
            int delay = extras.getInt("delay") * 1000;

            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String pathToInfoFile = getFilesDir() + File.separator + "info.txt";
            cStatesFile = getFilesDir() + File.separator + "CStates.csv";
            System.out.println("ioiiiiiiiii1" + cStatesFile);

            int duration = extras.getInt("testDuration");
            cStatesValuesFile = getFilesDir() + File.separator
                    + "CStatesValues.csv";
            DeviceInfo.getCstates(cStatesFile, duration, pathToInfoFile,
                    cStatesValuesFile);
            System.out.println("ioiiiiiiii2" + cStatesFile);

            return "done!";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null)
                dialog.dismiss();

            TextView title = new TextView(CstatesActivity.this);
            String thetitle = "Test complete. Path to results file - "
                    + cStatesFile;
            title.setText(thetitle);
            title.setGravity(Gravity.CENTER);
            title.setTypeface(Typeface.SERIF, Typeface.BOLD);
            title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);

            TableLayout table = (TableLayout) CstatesActivity.this
                    .findViewById(R.id.general_info_table);

            table.setStretchAllColumns(true);
            table.setScrollContainer(true);

            TableRow rowTitle = new TableRow(CstatesActivity.this);
            TableRow.LayoutParams params = new TableRow.LayoutParams();
            params.span = 6;
            params.setMargins(15, 15, 15, 15);
            rowTitle.addView(title, params);
            table.addView(rowTitle);
            table.addView(graphButton);

            try {
                BufferedReader generalInformationReader = new
                        BufferedReader(new FileReader(new File(cStatesFile)));
                String currentLine;

                while ((currentLine = generalInformationReader.readLine()) !=
                        null) {
                    TableRow currentRow = new TableRow(CstatesActivity.this);
                    String[] currentSplitLine = currentLine.split(",");
                    TextView[] currentLineTexts = new
                            TextView[currentSplitLine.length];

                    for (int i = 0; i < currentSplitLine.length; i++) {
                        currentLineTexts[i] = new
                                TextView(CstatesActivity.this);
                        currentLineTexts[i].setText(currentSplitLine[i]);
                        currentLineTexts[i].setTextSize(TypedValue.COMPLEX_UNIT_DIP,
                                14);
                        currentLineTexts[i].setTypeface(Typeface.SERIF);
                    }

                    for (int i = 0; i < currentLineTexts.length; i++) {
                        currentRow.addView(currentLineTexts[i]);
                    }
                    table.addView(currentRow);
                }

                generalInformationReader.close();

                BufferedReader cStatesValuesReader = new BufferedReader(new
                        FileReader(new File(cStatesValuesFile)));

                currentLine = "";
                while ((currentLine = cStatesValuesReader.readLine()) != null)
                {
                    String[] currentSplitLine = currentLine.split(",");
                    ArrayList<String> currentLineInList = new
                            ArrayList<String>();

                    for (int i = 0; i < currentSplitLine.length; i++) {
                        currentLineInList.add(currentSplitLine[i]);
                    }
                    dataFromFile.add(currentLineInList);

                }
                cStatesValuesReader.close();

            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

    }
}
