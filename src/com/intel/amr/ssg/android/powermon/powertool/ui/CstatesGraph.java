package com.intel.amr.ssg.android.powermon.powertool.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.LinearLayout;
import com.intel.amr.ssg.android.powermon.R;
import com.intel.amr.ssg.android.powermon.graphtools.BarGraphView;
import com.intel.amr.ssg.android.powermon.graphtools.GraphView;
import com.intel.amr.ssg.android.powermon.graphtools.GraphView.GraphViewData;
import com.intel.amr.ssg.android.powermon.graphtools.GraphViewSeries;
import com.intel.amr.ssg.android.powermon.graphtools.GraphViewSeries.GraphViewStyle;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@SuppressLint("ResourceAsColor")
public class CstatesGraph extends Activity {
    ProgressDialog dialog;
    GraphView graphView;
    GraphView core0Graph, core1Graph, core2Graph, core3Graph;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.cstates_graph);
        final CollectTheCStates collector = new CollectTheCStates();
        collector.execute("none");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.cstates_graph);
    }

    public class CollectTheCStates extends AsyncTask<String, Integer, String> {
        public List<String> OnlineCores = new ArrayList<String>();
        public List<Long> TheTime = new ArrayList<Long>();

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(CstatesGraph.this);
            dialog.setIndeterminate(true);
            dialog.setCancelable(false);
            dialog.setMessage("Collecting Data! Please wait....!");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Bundle extras = getIntent().getExtras();
            int theduration = extras.getInt("testDuration");
            // String[][] CStatesTable = extras.getStringArray("CStatesTable");

            ArrayList<String> Col1 = extras.getStringArrayList("Col1");
            ArrayList<String> Col2 = extras.getStringArrayList("Col2");
            ArrayList<String> Col3 = extras.getStringArrayList("Col3");

            HashSet<String> stateSet = new HashSet<String>();
            stateSet.addAll(Col3);

            core0Graph = new BarGraphView(getBaseContext(), "Core 0");
            core1Graph = new BarGraphView(getBaseContext(), "Core 1");
            core2Graph = new BarGraphView(getBaseContext(), "Core 2");
            core3Graph = new BarGraphView(getBaseContext(), "Core 3");

            HashSet<String> coreSet = new HashSet<String>();
            coreSet.addAll(Col3);

            for (int k = 0; k < 4; k++) {
                GraphViewData[] currentCoreStates = new GraphViewData[6];

                currentCoreStates[0] = new GraphViewData(1,
                        Double.parseDouble(Col2.get(0 + (k * 6))));
                currentCoreStates[1] = new GraphViewData(1,
                        Double.parseDouble(Col2.get(1 + (k * 6))));
                currentCoreStates[2] = new GraphViewData(1,
                        Double.parseDouble(Col2.get(2 + (k * 6))));
                currentCoreStates[3] = new GraphViewData(1,
                        Double.parseDouble(Col2.get(3 + (k * 6))));
                currentCoreStates[4] = new GraphViewData(1,
                        Double.parseDouble(Col2.get(4 + (k * 6))));
                currentCoreStates[5] = new GraphViewData(1,
                        Double.parseDouble(Col2.get(5 + (k * 6))));

                GraphViewSeries currentCore = new GraphViewSeries("Core " + k,
                        new GraphViewStyle(Color.rgb(00, 191, 255), 3),
                        currentCoreStates);
                if (k == 0) {
                    core0Graph.addSeries(currentCore);
                } else if (k == 1) {
                    core1Graph.addSeries(currentCore);
                } else if (k == 2) {
                    core2Graph.addSeries(currentCore);
                } else if (k == 3) {
                    core3Graph.addSeries(currentCore);
                }

            }

            // core0Graph
            // .setHorizontalLabels(new String[] { "State0", "State1",
            // "State2", "State3", "State4", "State5", "State6",
            // "State7" });
            // core1Graph
            // .setHorizontalLabels(new String[] { "State0", "State1",
            // "State2", "State3", "State4", "State5", "State6",
            // "State7" });
            // core2Graph
            // .setHorizontalLabels(new String[] { "State0", "State1",
            // "State2", "State3", "State4", "State5", "State6",
            // "State7" });
            // core3Graph
            // .setHorizontalLabels(new String[] { "State0", "State1",
            // "State2", "State3", "State4", "State5", "State6",
            // "State7" });

            core1Graph.setHorizontalLabels(new String[] { "C0", "C1", "C1E",
                    "C3", "C6", "C7" });
            core1Graph.setHorizontalLabels(new String[] { "C0", "C1", "C1E",
                    "C3", "C6", "C7" });
            core2Graph.setHorizontalLabels(new String[] { "C0", "C1", "C1E",
                    "C3", "C6", "C7" });
            core3Graph.setHorizontalLabels(new String[] { "C0", "C1", "C1E",
                    "C3", "C6", "C7" });

            return "Done!";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                dialog.dismiss();
            }

            setContentView(R.layout.cstates_graph);
            LinearLayout layout = (LinearLayout)
                    CstatesGraph.this.findViewById(R.id.graphline1);
            layout.setBackgroundColor(Color.BLACK);
            layout.addView(core0Graph);

            LinearLayout layout1 = (LinearLayout)
                    CstatesGraph.this.findViewById(R.id.graphline2);
            layout1.setBackgroundColor(Color.BLACK);
            layout1.addView(core1Graph);

            LinearLayout layout2 = (LinearLayout)
                    CstatesGraph.this.findViewById(R.id.graphline3);
            layout2.setBackgroundColor(Color.BLACK);
            layout2.addView(core2Graph);

            LinearLayout layout3 = (LinearLayout)
                    CstatesGraph.this.findViewById(R.id.graphline4);
            layout3.setBackgroundColor(Color.BLACK);
            layout3.addView(core3Graph);
        }
    }
}
