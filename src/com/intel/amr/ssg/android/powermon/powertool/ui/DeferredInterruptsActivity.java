package com.intel.amr.ssg.android.powermon.powertool.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.intel.amr.ssg.android.powermon.R;
import com.intel.amr.ssg.android.powermon.powertool.backend.DeviceInfo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class DeferredInterruptsActivity extends Activity {
    ProgressDialog dialog;
    Button graphButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.general_information);

        graphButton = new Button(DeferredInterruptsActivity.this);
        graphButton.setText("View graph");
        final DeferredInterruptsCollector collector = new
                DeferredInterruptsCollector();
        graphButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent GraphScreen = new Intent(DeferredInterruptsActivity.this,
                        TicklessInterruptsGraph.class);

                GraphScreen.putExtra("testDuration", getIntent().getExtras()
                        .getInt("testDuration"));

                ArrayList<ArrayList<String>> dataFromFile = new
                        ArrayList<ArrayList<String>>();
                BufferedReader ticklessValuesReader;

                try {
                    ticklessValuesReader = new BufferedReader(new
                            FileReader(new
                                    File(collector.pathToDeferredInterruptsFile)));
                    String currentLine = "";
                    currentLine = ticklessValuesReader.readLine();
                    currentLine = ticklessValuesReader.readLine();

                    while ((currentLine = ticklessValuesReader.readLine()) !=
                            null) {
                        String[] currentSplitLine = currentLine.split(",");
                        ArrayList<String> currentLineInList = new
                                ArrayList<String>();
                        for (int i = 0; i < currentSplitLine.length; i++) {
                            currentLineInList.add(currentSplitLine[i]);
                        }

                        if (!(currentLineInList.get(0).contains("total"))
                                && !(currentLineInList.get(0).contains("D"))) {
                            dataFromFile.add(currentLineInList);
                        }
                    }

                    ticklessValuesReader.close();
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                ArrayList<ArrayList<String>> correctTicklessList = new
                        ArrayList<ArrayList<String>>();

                for (int i = 0; i < dataFromFile.size(); i++) {
                    if (!(dataFromFile.get(i).size() < 5)) {
                        correctTicklessList.add(dataFromFile.get(i));
                    }
                }

                ArrayList<String> Col11 = new ArrayList<String>();
                ArrayList<String> Col21 = new ArrayList<String>();

                for (ArrayList<String> s : correctTicklessList) {
                    Col11.add(s.get(0));
                    Col21.add(s.get(2));
                }

                GraphScreen.putStringArrayListExtra("Col1", Col11);
                GraphScreen.putStringArrayListExtra("Col2", Col21);
                startActivity(GraphScreen);

            }
        });

        collector.execute("none");
    }

    public class DeferredInterruptsCollector extends
            AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(DeferredInterruptsActivity.this);
            dialog.setIndeterminate(true);
            dialog.setCancelable(false);
            dialog.setMessage("Please wait for the results.....");
            dialog.show();

        }

        String pathToDeferredInterruptsFile;
        String pathToDeferredInterruptsValuesFile;

        @Override
        protected String doInBackground(String... params) {
            Bundle extras = getIntent().getExtras();
            int delay = extras.getInt("delay") * 1000;
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            pathToDeferredInterruptsFile = getFilesDir() + File.separator
                    + "deferred_interrupts.csv";
            pathToDeferredInterruptsValuesFile = getFilesDir() + File.separator
                    + "deferred_interrupts_values.csv";
            System.out.println("ioiiiiiiiii1" + pathToDeferredInterruptsFile);
            // Bundle extras = getIntent().getExtras();
            int duration = extras.getInt("testDuration");
            DeviceInfo.getTicklessInterrupts(pathToDeferredInterruptsFile,
                    duration, pathToDeferredInterruptsValuesFile, true);
            System.out.println("ioiiiiiiii2" + pathToDeferredInterruptsFile);

            return "done!";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                dialog.dismiss();
            }

            TextView title = new TextView(DeferredInterruptsActivity.this);
            String thetitle = "Test complete. Path to results file - "
                    + pathToDeferredInterruptsFile;
            title.setText(thetitle);
            title.setGravity(Gravity.CENTER);
            title.setTypeface(Typeface.SERIF, Typeface.BOLD);
            title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);

            TableLayout table = (TableLayout) DeferredInterruptsActivity.this
                    .findViewById(R.id.general_info_table);

            table.setStretchAllColumns(true);
            table.setScrollContainer(true);

            TableRow rowTitle = new TableRow(DeferredInterruptsActivity.this);
            TableRow.LayoutParams params = new TableRow.LayoutParams();
            params.span = 6;
            params.setMargins(15, 15, 15, 15);
            rowTitle.addView(title, params);
            table.addView(rowTitle);
            table.addView(graphButton);

            try {
                BufferedReader generalInformationReader = new
                        BufferedReader(new FileReader(new
                                File(pathToDeferredInterruptsFile)));
                String currentLine;
                while ((currentLine = generalInformationReader.readLine()) !=
                        null) {
                    TableRow currentRow = new
                             TableRow(DeferredInterruptsActivity.this);
                    String[] currentSplitLine = currentLine.split(",");
                    TextView[] currentLineTexts = new
                            TextView[currentSplitLine.length];
                    for (int i = 0; i < currentSplitLine.length; i++) {
                        currentLineTexts[i] = new
                                TextView(DeferredInterruptsActivity.this);
                        currentLineTexts[i].setText(currentSplitLine[i]);
                        currentLineTexts[i].setTextSize(TypedValue.COMPLEX_UNIT_DIP,
                                14);
                        currentLineTexts[i].setTypeface(Typeface.SERIF);
                    }

                    for (int i = 0; i < currentLineTexts.length; i++) {
                        currentRow.addView(currentLineTexts[i]);
                    }
                    table.addView(currentRow);
                }
                generalInformationReader.close();

            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
