package com.intel.amr.ssg.android.powermon.powertool.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.intel.amr.ssg.android.powermon.R;
import com.intel.amr.ssg.android.powermon.powertool.backend.DeviceInfo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class GeneralInformationActivity extends Activity {
    ProgressDialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.general_information);

        final GeneralInformationCollector collector = new
                GeneralInformationCollector();
        collector.execute("none");
    }

    public class GeneralInformationCollector extends
            AsyncTask<String, Integer, String> {
        String pathToGeneralInformationFile;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(GeneralInformationActivity.this);
            dialog.setIndeterminate(true);
            dialog.setCancelable(false);
            dialog.setMessage("Please wait for the results.....");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String pathToInfoFile = getFilesDir() + File.separator + "info.txt";
            pathToGeneralInformationFile = getFilesDir() + File.separator
                    + "GeneralInformation.csv";
            System.out.println("ioiiiiiiiii" + pathToInfoFile);
            ArrayList<Integer> coresList = DeviceInfo.checkCores();
            DeviceInfo.getCpuCoresInfoInFile(coresList, pathToInfoFile);
            DeviceInfo.getGeneralCstatesInformation(coresList, pathToInfoFile,
                    pathToGeneralInformationFile);
            DeviceInfo.getGeneralPstatesInformation(
                    pathToGeneralInformationFile);
            DeviceInfo.getAcpiWakeupInformation(pathToGeneralInformationFile);

            return "done!";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                dialog.dismiss();
            }

            TextView title = new TextView(GeneralInformationActivity.this);
            String thetitle = "Test complete. Path to results file - "
                    + pathToGeneralInformationFile;
            title.setText(thetitle);
            title.setGravity(Gravity.CENTER);
            title.setTypeface(Typeface.SERIF, Typeface.BOLD);
            title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);

            TableLayout table = (TableLayout) GeneralInformationActivity.this
                    .findViewById(R.id.general_info_table);

            table.setStretchAllColumns(true);
            table.setScrollContainer(true);

            TableRow rowTitle = new TableRow(GeneralInformationActivity.this);
            TableRow.LayoutParams params = new TableRow.LayoutParams();
            params.span = 6;
            params.setMargins(15, 15, 15, 15);
            rowTitle.addView(title, params);
            table.addView(rowTitle);

            try {
                BufferedReader generalInformationReader = new
                        BufferedReader(new FileReader(new
                                File(pathToGeneralInformationFile)));
                String currentLine;

                while ((currentLine = generalInformationReader.readLine()) !=
                        null) {
                    TableRow currentRow = new
                            TableRow(GeneralInformationActivity.this);
                    String[] currentSplitLine = currentLine.split(",");
                    TextView[] currentLineTexts = new
                            TextView[currentSplitLine.length];

                    for (int i = 0; i < currentSplitLine.length; i++) {

                        currentLineTexts[i] = new
                                TextView(GeneralInformationActivity.this);
                        currentLineTexts[i].setText(currentSplitLine[i]);
                        currentLineTexts[i].setTextSize(TypedValue.COMPLEX_UNIT_DIP,
                                14);
                        currentLineTexts[i].setTypeface(Typeface.SERIF);

                    }

                    for (int i = 0; i < currentLineTexts.length; i++) {
                        currentRow.addView(currentLineTexts[i]);
                    }

                    table.addView(currentRow);
                }
                generalInformationReader.close();
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
