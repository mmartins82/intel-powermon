package com.intel.amr.ssg.android.powermon.powertool.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.LinearLayout;
import com.intel.amr.ssg.android.powermon.R;
import com.intel.amr.ssg.android.powermon.graphtools.GraphView;
import com.intel.amr.ssg.android.powermon.graphtools.GraphView.GraphViewData;
import com.intel.amr.ssg.android.powermon.graphtools.GraphViewSeries;
import com.intel.amr.ssg.android.powermon.graphtools.GraphViewSeries.GraphViewStyle;
import com.intel.amr.ssg.android.powermon.graphtools.LineGraphView;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("ResourceAsColor")
public class InterruptsGraph extends Activity {
    ProgressDialog dialog;
    GraphView totalInterruptsGraph, core0lInterruptsGraph,
            core1lInterruptsGraph, core2lInterruptsGraph,
            core3lInterruptsGraph;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.cstates_graph);
        final CollectTheInterrupts collector = new CollectTheInterrupts();
        collector.execute("none");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.cstates_graph);
    }

    public class CollectTheInterrupts extends
            AsyncTask<String, Integer, String> {

        public List<String> OnlineCores = new ArrayList<String>();
        public List<Long> TheTime = new ArrayList<Long>();

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(InterruptsGraph.this);
            dialog.setIndeterminate(true);
            dialog.setCancelable(false);
            dialog.setMessage("Collecting Data! Please wait....!");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Bundle extras = getIntent().getExtras();
            // int theduration = extras.getInt("testDuration");
            ArrayList<String> ColInterruptsName =
                    extras.getStringArrayList("ColInterruptName");
            ArrayList<String> ColCore0 = extras.getStringArrayList("ColCore0");
            ArrayList<String> ColCore1 = extras.getStringArrayList("ColCore1");
            ArrayList<String> ColCore2 = extras.getStringArrayList("ColCore2");
            ArrayList<String> ColCore3 = extras.getStringArrayList("ColCore3");
            ArrayList<String> ColTotal = extras.getStringArrayList("ColTotal");

            totalInterruptsGraph = new LineGraphView(getBaseContext(),
                    "Interrupts - total");
            core0lInterruptsGraph = new LineGraphView(getBaseContext(),
                    "Interrupts = core0");
            core1lInterruptsGraph = new LineGraphView(getBaseContext(),
                    "Interrupts = core1");
            core2lInterruptsGraph = new LineGraphView(getBaseContext(),
                    "Interrupts = core2");
            core3lInterruptsGraph = new LineGraphView(getBaseContext(),
                    "Interrupts = core3");

            String[] InterruptNames = new String[ColInterruptsName.size()];
            InterruptNames = ColInterruptsName.toArray(InterruptNames);
            totalInterruptsGraph.setHorizontalLabels(InterruptNames);
            core0lInterruptsGraph.setHorizontalLabels(InterruptNames);
            core1lInterruptsGraph.setHorizontalLabels(InterruptNames);
            core2lInterruptsGraph.setHorizontalLabels(InterruptNames);
            core3lInterruptsGraph.setHorizontalLabels(InterruptNames);

            GraphViewData[] currentInterruptScores = new
                    GraphViewData[ColInterruptsName.size()];

            for (int k = 0; k < currentInterruptScores.length; k++) {
                currentInterruptScores[k] = new GraphViewData(k,
                        Double.parseDouble(ColTotal.get(k)));
            }

            GraphViewSeries currentCore = new GraphViewSeries("Interrupts",
                    new GraphViewStyle(Color.rgb(00, 191, 255), 3),
                    currentInterruptScores);
            totalInterruptsGraph.addSeries(currentCore);

            GraphViewData[] currentInterruptScores1 = new
                GraphViewData[ColInterruptsName.size()];
            for (int k = 0; k < currentInterruptScores1.length; k++) {
                currentInterruptScores1[k] = new GraphViewData(k,
                        Integer.parseInt(ColCore0.get(k)));
            }

            GraphViewSeries currentCore1 = new GraphViewSeries("Interrupts",
                    new GraphViewStyle(Color.rgb(00, 191, 255), 3),
                    currentInterruptScores1);
            core0lInterruptsGraph.addSeries(currentCore1);

            GraphViewData[] currentInterruptScores2 = new
                    GraphViewData[ColInterruptsName.size()];
            for (int k = 0; k < currentInterruptScores2.length; k++) {
                currentInterruptScores2[k] = new GraphViewData(k,
                        Integer.parseInt(ColCore1.get(k)));
            }

            GraphViewSeries currentCore2 = new GraphViewSeries("Interrupts",
                    new GraphViewStyle(Color.rgb(00, 191, 255), 3),
                    currentInterruptScores2);
            core1lInterruptsGraph.addSeries(currentCore2);

            GraphViewData[] currentInterruptScores3 = new
                    GraphViewData[ColInterruptsName.size()];
            for (int k = 0; k < currentInterruptScores3.length; k++) {
                currentInterruptScores3[k] = new GraphViewData(k,
                        Integer.parseInt(ColCore2.get(k)));
            }

            GraphViewSeries currentCore3 = new GraphViewSeries("Interrupts",
                    new GraphViewStyle(Color.rgb(00, 191, 255), 3),
                    currentInterruptScores3);
            core2lInterruptsGraph.addSeries(currentCore3);

            GraphViewData[] currentInterruptScores4 = new
                    GraphViewData[ColInterruptsName.size()];
            for (int k = 0; k < currentInterruptScores4.length; k++) {
                currentInterruptScores4[k] = new GraphViewData(k,
                        Integer.parseInt(ColCore3.get(k)));
            }

            GraphViewSeries currentCore4 = new GraphViewSeries("Interrupts",
                    new GraphViewStyle(Color.rgb(00, 191, 255), 3),
                    currentInterruptScores4);
            core3lInterruptsGraph.addSeries(currentCore4);

            return "Done!";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                dialog.dismiss();
            }

            setContentView(R.layout.interrupts_graph);
            LinearLayout layout = (LinearLayout)
                    InterruptsGraph.this.findViewById(R.id.interrupt_graphline1);
            layout.setBackgroundColor(Color.BLACK);
            layout.addView(totalInterruptsGraph);

            LinearLayout layout1 = (LinearLayout)
                    InterruptsGraph.this.findViewById(R.id.interrupt_graphline2);
            layout1.setBackgroundColor(Color.BLACK);
            layout1.addView(core0lInterruptsGraph);

            LinearLayout layout2 = (LinearLayout)
                    InterruptsGraph.this.findViewById(R.id.interrupt_graphline3);
            layout2.setBackgroundColor(Color.BLACK);
            layout2.addView(core1lInterruptsGraph);

            LinearLayout layout3 = (LinearLayout)
                    InterruptsGraph.this.findViewById(R.id.interrupt_graphline4);
            layout3.setBackgroundColor(Color.BLACK);
            layout3.addView(core2lInterruptsGraph);

            LinearLayout layout4 = (LinearLayout)
                    InterruptsGraph.this.findViewById(R.id.interrupt_graphline5);
            layout4.setBackgroundColor(Color.BLACK);
            layout4.addView(core3lInterruptsGraph);
        }
    }
}
