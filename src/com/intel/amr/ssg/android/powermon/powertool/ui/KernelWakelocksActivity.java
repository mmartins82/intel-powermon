package com.intel.amr.ssg.android.powermon.powertool.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.asksven.android.common.kernelutils.NativeKernelWakelock;
import com.intel.amr.ssg.android.powermon.R;
import com.intel.amr.ssg.android.powermon.powertool.backend.KernelWakelockTask;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * User: Marcelo Martins <martins@cs.brown.edu>
 * Date: 12/27/13
 * Time: 10:49 AM
 */
public class KernelWakelocksActivity extends Activity {
        ProgressDialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.general_information);

        final KernelWakelockCollector collector = new KernelWakelockCollector();
        collector.execute("none");
    }

    public class KernelWakelockCollector extends
           AsyncTask<String, Integer, String> {
        String pathKernelWakelocksFile = getFilesDir() + File.separator
                + "kernel_wakelocks.txt";
        Future<List<NativeKernelWakelock>> futureWakelocks;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(KernelWakelocksActivity.this);
            dialog.setIndeterminate(true);
            dialog.setCancelable(false);
            dialog.setMessage("Please wait for the results.....");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Bundle extras = getIntent().getExtras();
            int delay = extras.getInt("delay") * 1000;
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            ExecutorService executor = Executors.newSingleThreadExecutor();
            KernelWakelockTask thread = new KernelWakelockTask();
            thread.setContext(KernelWakelocksActivity.this);
            thread.setOutputFile(pathKernelWakelocksFile);
            futureWakelocks = executor.submit(thread);

            return "done!";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                dialog.dismiss();
            }

            TextView titleView = new TextView(KernelWakelocksActivity.this);
            String title = "Test complete. Path to results file - "
                    + pathKernelWakelocksFile;
            titleView.setText(title);
            titleView.setGravity(Gravity.CENTER);
            titleView.setTypeface(Typeface.SERIF, Typeface.BOLD);
            titleView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);

            TableLayout table = (TableLayout)
                    KernelWakelocksActivity.this.findViewById(R.id.general_info_table);

            table.setStretchAllColumns(true);
            table.setScrollContainer(true);

            TableRow rowTitle = new TableRow(KernelWakelocksActivity.this);
            TableRow.LayoutParams params = new TableRow.LayoutParams();
            params.span = 6;
            params.setMargins(15, 15, 15, 15);
            rowTitle.addView(titleView, params);
            table.addView(rowTitle);

            List<NativeKernelWakelock> kernelWakelocks = null;
            try {
                kernelWakelocks = futureWakelocks.get();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (kernelWakelocks != null) {
                for (NativeKernelWakelock wl : kernelWakelocks) {
                    TableRow row = new TableRow(KernelWakelocksActivity.this);

                    // Process UID
                    TextView field = new TextView(KernelWakelocksActivity.this);
                    field.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
                    field.setTypeface(Typeface.MONOSPACE);
                    field.setText(Integer.toString(wl.getUidInfo().getUid()));
                    row.addView(field);

                    // Wakelock name
                    field = new TextView(KernelWakelocksActivity.this);
                    field.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
                    field.setTypeface(Typeface.MONOSPACE);
                    field.setText(wl.getName());
                    row.addView(field);

                    // Wakelock count
                    field = new TextView(KernelWakelocksActivity.this);
                    field.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
                    field.setTypeface(Typeface.MONOSPACE);
                    field.setText(Integer.toString(wl.getCount()));
                    row.addView(field);

                    // Wakelock duration
                    field = new TextView(KernelWakelocksActivity.this);
                    field.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
                    field.setTypeface(Typeface.MONOSPACE);
                    field.setText(Long.toString(wl.getDuration()));
                    row.addView(field);

                    table.addView(row);
                }
            }
        }
    }
}
