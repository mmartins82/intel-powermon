package com.intel.amr.ssg.android.powermon.powertool.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.RadioGroup;
import com.intel.amr.ssg.android.powermon.R;

public class MainActivity extends Activity {
    ProgressDialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_tests);
        Button StartTest = (Button) this.findViewById(R.id.starttest);
        NumberPicker np = (NumberPicker) this.findViewById(R.id.numberpicker);
        np.setMinValue(1);
        np.setMaxValue(1000);
        NumberPicker delayPicker = (NumberPicker)
               this.findViewById(R.id.delayPicker);
        delayPicker.setMinValue(0);
        delayPicker.setMaxValue(50);

        StartTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NumberPicker numPicker = (NumberPicker)
                        findViewById(R.id.numberpicker);
                NumberPicker delayPickerValue = (NumberPicker)
                        findViewById(R.id.delayPicker);
                int theduration = numPicker.getValue();
                int delay = delayPickerValue.getValue();

                RadioGroup g = (RadioGroup) findViewById(R.id.testscenario);
                switch (g.getCheckedRadioButtonId()) {
                    case R.id.general_information:
                        Intent ScreenForGeneralInformation = new Intent(getApplicationContext(), GeneralInformationActivity.class);
                        ScreenForGeneralInformation.putExtra("testDuration",
                                theduration);
                        ScreenForGeneralInformation.putExtra("delay",
                                delay);
                        startActivity(ScreenForGeneralInformation);
                        break;

                    case R.id.idle_states:
                        Intent ScreenForCStates = new Intent(getApplicationContext(), CstatesActivity.class);
                        ScreenForCStates.putExtra("testDuration",
                                theduration);
                        ScreenForCStates.putExtra("delay", delay);
                        startActivity(ScreenForCStates);
                        break;

                    case R.id.interrupts:
                        Intent ScreenForInterrupts = new Intent(getApplicationContext(), InterruptsActivity.class);
                        ScreenForInterrupts.putExtra("testDuration",
                                theduration);
                        ScreenForInterrupts.putExtra("delay",
                                delay);
                        startActivity(ScreenForInterrupts);
                        break;

                    case R.id.software_interrupts:
                        Intent ScreenForSoftwareInterrupts = new Intent(getApplicationContext(), SoftwareInterruptsActivity.class);
                        ScreenForSoftwareInterrupts.putExtra("testDuration",
                                theduration);
                        ScreenForSoftwareInterrupts.putExtra("delay",
                                delay);
                        startActivity(ScreenForSoftwareInterrupts);
                        break;

                    case R.id.tickless_interrupts:
                        Intent ScreenForTicklessInterrupts = new Intent(getApplicationContext(), TicklessInterruptsActivity.class);
                        ScreenForTicklessInterrupts.putExtra("testDuration",
                                theduration);
                        ScreenForTicklessInterrupts.putExtra("delay",
                                delay);
                        startActivity(ScreenForTicklessInterrupts);
                        break;

                    case R.id.deferred_interrupts:
                        Intent ScreenForDeferredInterrupts = new Intent(getApplicationContext(), DeferredInterruptsActivity.class);
                        ScreenForDeferredInterrupts.putExtra("testDuration",
                                theduration);
                        ScreenForDeferredInterrupts.putExtra("delay",
                                delay);
                        startActivity(ScreenForDeferredInterrupts);
                        break;

                    case R.id.io_analysis:
                        Intent ScreenForIOAnalysis = new Intent(getApplicationContext(), IoAnalysisActivity.class);
                        ScreenForIOAnalysis.putExtra("testDuration",
                                theduration);
                        ScreenForIOAnalysis.putExtra("delay",
                                delay);
                        startActivity(ScreenForIOAnalysis);
                        break;

                    case R.id.device_power_analysis:
                        Intent ScreenForDevicePowerAnalysis = new Intent(getApplicationContext(), DevicePowerActivity.class);
                        ScreenForDevicePowerAnalysis.putExtra("testDuration",
                                theduration);
                        ScreenForDevicePowerAnalysis.putExtra("delay",
                                delay);
                        startActivity(ScreenForDevicePowerAnalysis);
                        break;

                    case R.id.ftrace_power_events:
                        Intent ScreenForFtracePowerEvents = new Intent(getApplicationContext(), FtracePowerActivity.class);
                        ScreenForFtracePowerEvents.putExtra("testDuration",
                                theduration);
                        ScreenForFtracePowerEvents.putExtra("delay",
                                delay);
                        startActivity(ScreenForFtracePowerEvents);
                        break;

                    case R.id.cpu_utilization:
                        Intent ScreenForCpuUtilization = new Intent(getApplicationContext(), CpuUtilizationActivity.class);
                        ScreenForCpuUtilization.putExtra("testDuration",
                                theduration);
                        ScreenForCpuUtilization.putExtra("delay",
                                delay);
                        startActivity(ScreenForCpuUtilization);
                        break;

                    case R.id.kernel_wakelocks:
                        Intent ScreenForKernelWakelocks = new Intent(getApplicationContext(), KernelWakelocksActivity.class);
                        ScreenForKernelWakelocks.putExtra("testDuration",
                                theduration);
                        ScreenForKernelWakelocks.putExtra("delay",
                                delay);
                        startActivity(ScreenForKernelWakelocks);
                        break;

                    case R.id.user_wakelocks:
                        Intent ScreenForUserWakelocks = new Intent(getApplicationContext(), UserWakelocksActivity.class);
                        ScreenForUserWakelocks.putExtra("testDuration",
                                theduration);
                        ScreenForUserWakelocks.putExtra("delay",
                                delay);
                        startActivity(ScreenForUserWakelocks);
                        break;

                    case R.id.update_governer:
                        Intent ScreenForUpdateGovernor = new Intent(getApplicationContext(), UpdateGovernorActivity.class);
                        startActivity(ScreenForUpdateGovernor);
                        break;

                    case R.id.gpu_residency:
                        Intent ScreenForGpuResidency = new Intent(getApplicationContext(), GpuResidencyActivity.class);
                        ScreenForGpuResidency.putExtra("testDuration",
                                theduration);
                        ScreenForGpuResidency.putExtra("delay",
                                delay);
                        startActivity(ScreenForGpuResidency);
                        break;

                    //case R.id.all_tests:
                    //    Intent ScreenForAllTests = new Intent(getApplicationContext(), AllTestsActivity.class);
                    //    ScreenForAllTests.putExtra("testDuration", theduration);
                    //    ScreenForAllTests.putExtra("delay", delay);
                    //    startActivity(ScreenForAllTests);
                    //    break;
                }
            }
        });
    }
}
