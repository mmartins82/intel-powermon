package com.intel.amr.ssg.android.powermon.powertool.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.intel.amr.ssg.android.powermon.R;
import com.intel.amr.ssg.android.powermon.powertool.backend.DeviceInfo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class SoftwareInterruptsActivity extends Activity {
    ProgressDialog dialog;
    Button graphButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.general_information);

        final SoftwareInterruptsCollector collector = new
                SoftwareInterruptsCollector();

        graphButton = new Button(SoftwareInterruptsActivity.this);
        graphButton.setText("View graph");

        graphButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent GraphScreen = new Intent(SoftwareInterruptsActivity.this,
                        InterruptsGraph.class);

                GraphScreen.putExtra("testDuration",
                        getIntent().getExtras().getInt("testDuration"));

                ArrayList<String> ColInterruptName = new ArrayList<String>();
                ArrayList<String> ColCore0 = new ArrayList<String>();
                ArrayList<String> ColCore1 = new ArrayList<String>();
                ArrayList<String> ColCore2 = new ArrayList<String>();
                ArrayList<String> ColCore3 = new ArrayList<String>();
                ArrayList<String> ColTotal = new ArrayList<String>();

                for (ArrayList<String> s : collector.dataFromFile) {
                    ColInterruptName.add(s.get(0));
                    ColCore0.add(s.get(1));
                    ColCore1.add(s.get(2));
                    ColCore2.add(s.get(3));
                    ColCore3.add(s.get(4));
                    ColTotal.add(s.get(5));
                }

                GraphScreen.putStringArrayListExtra("ColInterruptName",
                        ColInterruptName);
                GraphScreen.putStringArrayListExtra("ColCore0", ColCore0);
                GraphScreen.putStringArrayListExtra("ColCore1", ColCore1);
                GraphScreen.putStringArrayListExtra("ColCore2", ColCore2);
                GraphScreen.putStringArrayListExtra("ColCore3", ColCore3);
                GraphScreen.putStringArrayListExtra("ColTotal", ColTotal);
                startActivity(GraphScreen);
            }
        });

        collector.execute("none");

    }

    public class SoftwareInterruptsCollector extends
            AsyncTask<String, Integer, String> {
        public ArrayList<ArrayList<String>> dataFromFile = new
                ArrayList<ArrayList<String>>();

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(SoftwareInterruptsActivity.this);
            dialog.setIndeterminate(true);
            dialog.setCancelable(false);
            dialog.setMessage("Please wait for the results.....");
            dialog.show();
        }

        String pathToSoftwareInterruptsFile;
        String softwareInterruptsValuesFile;

        @Override
        protected String doInBackground(String... params) {
            Bundle extras = getIntent().getExtras();
            int delay = extras.getInt("delay") * 1000;
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            pathToSoftwareInterruptsFile = getFilesDir() + File.separator
                    + "SoftwareInterrupts.csv";
            softwareInterruptsValuesFile = getFilesDir() + File.separator
                    + "SoftwareInterruptsValues.csv";
            System.out.println("ioiiiiiiiii1" + pathToSoftwareInterruptsFile);
            // Bundle extras = getIntent().getExtras();
            int duration = extras.getInt("testDuration");
            DeviceInfo.getSoftwareInterrupts(pathToSoftwareInterruptsFile,
                    duration, softwareInterruptsValuesFile);
            System.out.println("ioiiiiiiii2" + pathToSoftwareInterruptsFile);

            return "done!";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null)
                dialog.dismiss();

            TextView title = new TextView(SoftwareInterruptsActivity.this);
            String thetitle = "Test complete. Path to results file - "
                    + pathToSoftwareInterruptsFile;
            title.setText(thetitle);
            title.setGravity(Gravity.CENTER);
            title.setTypeface(Typeface.SERIF, Typeface.BOLD);
            title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);

            TableLayout table = (TableLayout)
                    SoftwareInterruptsActivity.this.findViewById(R.id.general_info_table);

            table.setStretchAllColumns(true);
            table.setScrollContainer(true);

            TableRow rowTitle = new TableRow(SoftwareInterruptsActivity.this);
            TableRow.LayoutParams params = new TableRow.LayoutParams();
            params.span = 6;
            params.setMargins(15, 15, 15, 15);
            rowTitle.addView(title, params);
            table.addView(rowTitle);
            table.addView(graphButton);

            try {
                BufferedReader generalInformationReader = new
                        BufferedReader(new FileReader(new
                                File(pathToSoftwareInterruptsFile)));

                String currentLine;
                while ((currentLine = generalInformationReader.readLine()) !=
                        null) {
                    TableRow currentRow = new
                            TableRow(SoftwareInterruptsActivity.this);
                    String[] currentSplitLine = currentLine.split(",");

                    TextView[] currentLineTexts = new
                            TextView[currentSplitLine.length];
                    for (int i = 0; i < currentSplitLine.length; i++) {
                        currentLineTexts[i] = new
                                TextView(SoftwareInterruptsActivity.this);
                        currentLineTexts[i].setText(currentSplitLine[i]);
                        currentLineTexts[i].setTextSize(TypedValue.COMPLEX_UNIT_DIP,
                                14);
                        currentLineTexts[i].setTypeface(Typeface.SERIF);
                    }

                    for (int i = 0; i < currentLineTexts.length; i++) {
                        currentRow.addView(currentLineTexts[i]);
                    }
                    table.addView(currentRow);
                }
                generalInformationReader.close();

                BufferedReader interruptsValuesReader = new BufferedReader(new
                        FileReader(new File(softwareInterruptsValuesFile)));

                currentLine = interruptsValuesReader.readLine();
                while ((currentLine = interruptsValuesReader.readLine()) !=
                        null) {
                    String[] currentSplitLine = currentLine.split(",");
                    ArrayList<String> currentLineInList = new
                            ArrayList<String>();

                    for (int i = 0; i < currentSplitLine.length; i++) {
                        currentLineInList.add(currentSplitLine[i]);
                    }
                    if (currentLineInList.size() > 1) {
                        dataFromFile.add(currentLineInList);
                    }
                }

                interruptsValuesReader.close();

                for (int i = 0; i < dataFromFile.size(); i++) {
                    Integer total =
                            Integer.parseInt(dataFromFile.get(i).get(1))
                            + Integer.parseInt(dataFromFile.get(i).get(2))
                            + Integer.parseInt(dataFromFile.get(i).get(3))
                            + Integer.parseInt(dataFromFile.get(i).get(4));
                    dataFromFile.get(i).add(total.toString());
                }

            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
