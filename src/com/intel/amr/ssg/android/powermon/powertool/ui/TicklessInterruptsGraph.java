package com.intel.amr.ssg.android.powermon.powertool.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.LinearLayout;
import com.intel.amr.ssg.android.powermon.R;
import com.intel.amr.ssg.android.powermon.graphtools.GraphView;
import com.intel.amr.ssg.android.powermon.graphtools.GraphView.GraphViewData;
import com.intel.amr.ssg.android.powermon.graphtools.GraphViewSeries;
import com.intel.amr.ssg.android.powermon.graphtools.GraphViewSeries.GraphViewStyle;
import com.intel.amr.ssg.android.powermon.graphtools.LineGraphView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

@SuppressLint("ResourceAsColor")
public class TicklessInterruptsGraph extends Activity {
    ProgressDialog dialog;
    GraphView ticklessGraph;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.cstates_graph);
        final CollectTheCStates collector = new CollectTheCStates();
        collector.execute("none");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.cstates_graph);
    }

    public class CollectTheCStates extends AsyncTask<String, Integer, String> {
        public List<String> OnlineCores = new ArrayList<String>();
        public List<Long> TheTime = new ArrayList<Long>();

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(TicklessInterruptsGraph.this);
            dialog.setIndeterminate(true);
            dialog.setCancelable(false);
            dialog.setMessage("Collecting Data! Please wait....!");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Bundle extras = getIntent().getExtras();
            // int theduration = extras.getInt("testDuration");
            ArrayList<String> Col1 = extras.getStringArrayList("Col1");
            ArrayList<String> Col2 = extras.getStringArrayList("Col2");

            HashMap<Integer, Integer> counterMap = new HashMap<Integer,
                    Integer>();

            int countGreaterThanTen = 0;
            for (int i = 0; i < Col1.size(); i++) {
                counterMap.put(i, Integer.parseInt(Col1.get(i)));
                if (Integer.parseInt(Col1.get(i)) > 10) {
                    countGreaterThanTen++;
                }
            }

            ArrayList<Integer> intValuesForCol1 = new ArrayList<Integer>();
            for (String s1 : Col1) {
                intValuesForCol1.add(Integer.parseInt(s1));
            }

            Collections.sort(intValuesForCol1, new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    return o2.compareTo(o1);
                }
            });

            HashMap<Integer, Integer> sortedValuesMap = new HashMap<Integer,
                    Integer>();

            for (int i = 0; i < intValuesForCol1.size(); i++) {
                sortedValuesMap.put(intValuesForCol1.get(i), i);
            }

            String[] ticklessInterruptNames = new String[Col2.size()];

            for (int i = 0; i < Col2.size(); i++) {
                ticklessInterruptNames[i] = "  ";
            }

            HashSet<Integer> includedValues = new HashSet<Integer>();

            for (int i = 0; i < Col2.size(); i++) {
                int positionInSortedList =
                        sortedValuesMap.get(Integer.parseInt(Col1.get(i)));
                if (positionInSortedList < 10
                        && !(includedValues.contains(Col1.get(i)))) {
                    ticklessInterruptNames[i] = Col2.get(i);
                    includedValues.add(Integer.parseInt(Col1.get(i)));
                } else {
                    ticklessInterruptNames[i] = "";
                    includedValues.add(Integer.parseInt(Col1.get(i)));
                }
            }

            System.out.println("h");
            ticklessInterruptNames = new String[countGreaterThanTen];
            int labelIndex = 0;

            for (int i = 0; i < Col2.size(); i++) {
                if (counterMap.get(i) > 10) {
                    ticklessInterruptNames[labelIndex] = Col2.get(i);
                    labelIndex++;
                }
            }

            // ticklessInterruptNames = Col2.toArray(ticklessInterruptNames);

            ticklessGraph = new LineGraphView(getBaseContext(),
                    "Tickless interrupts");
            ticklessGraph.setHorizontalLabels(ticklessInterruptNames);
            GraphViewData[] currentTicklessScores = new
                    GraphViewData[countGreaterThanTen];

            int graphIndex = 0;
            for (int k = 0; k < Col1.size(); k++) {
                if (counterMap.get(k) > 10) {
                    currentTicklessScores[graphIndex] = new
                            GraphViewData(graphIndex,
                                    Integer.parseInt(Col1.get(k)));
                    graphIndex++;
                }

            }

            GraphViewSeries currentCore = new GraphViewSeries("Tickless "
                    + "Interrupts", new GraphViewStyle(Color.rgb(00, 191, 255),
                            3), currentTicklessScores);

            ticklessGraph.addSeries(currentCore);

            return "Done!";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                dialog.dismiss();
            }

            setContentView(R.layout.pstates_graph);
            LinearLayout layout = (LinearLayout)
                    TicklessInterruptsGraph.this.findViewById(R.id.pstatesgraphline);
            layout.setBackgroundColor(Color.BLACK);
            layout.addView(ticklessGraph);
        }
    }
}
