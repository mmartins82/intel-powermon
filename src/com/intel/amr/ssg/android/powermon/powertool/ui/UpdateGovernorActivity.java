package com.intel.amr.ssg.android.powermon.powertool.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.intel.amr.ssg.android.powermon.R;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class UpdateGovernorActivity extends Activity {

    public UpdateGovernorActivity() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // View rootView = inflater.inflate(R.layout.update_governor,
        // container, false);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_governor);

        RadioGroup radio_gov = (RadioGroup)
                UpdateGovernorActivity.this.findViewById(R.id.govoptions);
        RadioButton element;
        List<String> GOVLIST = new ArrayList<String>();

        try {
            String str = "";
            String availablegov = "";
            InputStream is = new
                    FileInputStream("/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors");
            BufferedReader reader = new BufferedReader(new
                    InputStreamReader(is));

            if (is != null) {
                while ((str = reader.readLine()) != null) {
                    String[] tokens = str.split(" ");
                    for (int i = 0; i < tokens.length; i++) {
                        GOVLIST.add(tokens[i]);
                    }
                }
            }

            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < GOVLIST.size(); i++) {
            element = new RadioButton(UpdateGovernorActivity.this);
            element.setText(GOVLIST.get(i));
            element.setTypeface(Typeface.SERIF);
            radio_gov.addView(element);
        }

        // Add Button Functionality for governor change
        Button ChangeGovButton = (Button)
                UpdateGovernorActivity.this.findViewById(R.id.button_changegov);

        ChangeGovButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioGroup g = (RadioGroup)
                        UpdateGovernorActivity.this.findViewById(R.id.govoptions);

                try {
                    AlertDialog.Builder alertgov = new
                            AlertDialog.Builder(UpdateGovernorActivity.this);
                    alertgov.setTitle("Changing Governor Confirmation");
                    Process process;
                    int id = g.getCheckedRadioButtonId();
                    View radiobutton = g.findViewById(id);
                    int radioId = g.indexOfChild(radiobutton);
                    RadioButton btn = (RadioButton) g.getChildAt(radioId);
                    String govtobechanged = (String) btn.getText();
                    String cmd = "echo " + govtobechanged
                            + "> /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor";

                    try {
                        process = Runtime.getRuntime().exec(new String[] {
                                "su", "-c", cmd });
                        process.waitFor();
                        int exitCode = process.exitValue();
                        String almessage = "";

                        if (exitCode == 0) {

                            almessage = "The governor has been changed to "
                                    + govtobechanged;
                        } else {
                            almessage = "Changing the governor to "
                                    + govtobechanged + " has failed!";
                        }

                        alertgov.setMessage(almessage);
                        alertgov.setCancelable(false);
                        alertgov.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                            int id) {
                                        dialog.dismiss();
                                    }
                                });
                        AlertDialog alertDialog = alertgov.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
