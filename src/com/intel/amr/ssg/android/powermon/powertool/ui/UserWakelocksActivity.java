package com.intel.amr.ssg.android.powermon.powertool.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.intel.amr.ssg.android.powermon.R;
import com.intel.amr.ssg.android.powermon.powertool.backend.UserWakelockTask;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * User: Marcelo Martins <martins@cs.brown.edu>
 * Date: 12/24/13
 * Time: 3:13 PM
 */
public class UserWakelocksActivity extends Activity {
    ProgressDialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.general_information);

        final UserWakelockCollector collector = new UserWakelockCollector();
        collector.execute("none");
    }

    public class UserWakelockCollector extends
            AsyncTask<String, Integer, String> {
        String pathUserWakelocksFile = getFilesDir() + File.separator
                + "user_wakelocks.txt";
        Future<List<String>> futureWakelocks;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(UserWakelocksActivity.this);
            dialog.setIndeterminate(true);
            dialog.setCancelable(false);
            dialog.setMessage("Please wait for the results.....");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Bundle extras = getIntent().getExtras();
            int delay = extras.getInt("delay") * 1000;
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            ExecutorService executor = Executors.newSingleThreadExecutor();
            UserWakelockTask thread = new UserWakelockTask();
            thread.setOutputFile(pathUserWakelocksFile);
            futureWakelocks = executor.submit(thread);

            return "done!";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                dialog.dismiss();
            }

            TextView titleView = new TextView(UserWakelocksActivity.this);
            String title = "Test complete. Path to results file - "
                    + pathUserWakelocksFile;
            titleView.setText(title);
            titleView.setGravity(Gravity.CENTER);
            titleView.setTypeface(Typeface.SERIF, Typeface.BOLD);
            titleView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);

            TableLayout table = (TableLayout)
                    UserWakelocksActivity.this.findViewById(R.id.general_info_table);

            table.setStretchAllColumns(true);
            table.setScrollContainer(true);

            TableRow rowTitle = new TableRow(UserWakelocksActivity.this);
            TableRow.LayoutParams params = new TableRow.LayoutParams();
            params.span = 6;
            params.setMargins(15, 15, 15, 15);
            rowTitle.addView(titleView, params);
            table.addView(rowTitle);

            List<String> userWakelocks = null;
            try {
                userWakelocks = futureWakelocks.get();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            for (String line : userWakelocks) {
                TableRow row = new TableRow(UserWakelocksActivity.this);
                TextView field = new TextView(UserWakelocksActivity.this);
                field.setText(line);
                field.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
                field.setTypeface(Typeface.MONOSPACE);
                row.addView(field);
                table.addView(row);
            }
        }
    }
}
