package com.intel.amr.ssg.android.powermon.powertool.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Marcelo Martins <martins@cs.brown.edu>
 * Date: 12/19/13
 * Time: 10:34 AM
 */
public class FileUtils {

    /**
     * Read contents from file
     *
     * @param filename  file name
     * @return file lines as list of string
     */
    protected static List<String> readFile(String filename) {
        assert (filename != null);

        List<String> contents = new ArrayList<String>();

        try {
            BufferedReader fin = new BufferedReader(new InputStreamReader(new
                    FileInputStream(filename)), 8192);

            String line;
            while ((line = fin.readLine()) != null)
                contents.add(line.trim());

            fin.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return contents;
    }

    /**
     * Write value to file (overwrite)
     *
     * @param filename  file name
     * @param value     value as string
     * @return whether write succeeded or not
     */
    protected static boolean writeFile(String filename, String value) {
        assert(filename != null);
        assert(value != null);

        try {
            BufferedWriter fout = new BufferedWriter(new OutputStreamWriter
                    (new FileOutputStream(filename)), 8192);

            // only write if value is non-empty
            if (value.length() > 0) {
                fout.write(value);
            }

            fout.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
