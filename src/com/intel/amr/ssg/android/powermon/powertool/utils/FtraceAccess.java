package com.intel.amr.ssg.android.powermon.powertool.utils;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;

/**
 * User: Marcelo Martins <martins@cs.brown.edu>
 * Date: 12/17/13
 * Java interface for controlling ftrace via sysfs
 */
public class FtraceAccess {

    private static String DEBUGFS_TRACING_ROOT = "/sys/kernel/debug/tracing/";
    private static boolean cachedTracers = false;
    private static boolean cachedEvents = false;
    private static boolean cachedCategories = false;
    private static String[] tracerNames = new String[0];
    private static String[] eventNames = new String[0];
    private static String[] eventCategories = new String[0];

    static String getTracingDir() {
        return DEBUGFS_TRACING_ROOT;
    }

    /**
     * Returns whether tracing is on or off
     *
     * @return trace is on or off (boolean)
     */
    public static boolean isTracingOn() {
        String res = SysFsAccess.readSysFsLine(getTracingDir()
                + "tracing_on", 0);
        return res.equals("1");
    }

    /**
     * Enables or disables system tracing
     *
     * @param enable enable/disable toggle
     */
    public static void enableTracing(boolean enable) {
        SysFsAccess.writeSysFs(getTracingDir() + "tracing_on",
                enable ? "1" : "0");
    }

    /**
     * Returns name of current tracer
     *
     * @return tracer name
     */
    public static String getTracer() {
        String line = SysFsAccess.readSysFsLine(getTracingDir()
                + "current_tracer", 0);
        return line.trim();
    }

    /**
     * Returns name list of available tracers
     *
     * @return list of available tracers
     */
    public static String[] getAvailableTracers() {
        // Since sysfs/ftrace does not change that often after system reboot,
        // let's cache tracers.
        // XXX: It may change after module load
        // QUESTION: how often are modules loaded in Android afte reboot?
        if (!cachedTracers) {
            String line = SysFsAccess.readSysFsLine(getTracingDir()
                    + "available_tracers", 0);
            tracerNames = line.trim().split("[ \t]+");
            // Sort to facilitate binary search later
            Arrays.sort(tracerNames);
        }

        return tracerNames;
    }

    /**
     * Returns list of available tracing events
     *
     * @return list of available events
     */
    public static String[] getAvailableEvents() {
        // Since sysfs/ftrace does not change that often after system reboot,
        // let's cache tracers.
        // XXX: It may change after module load
        // QUESTION: how often are modules loaded in Android after reboot?
        if (!cachedEvents) {
            eventNames = SysFsAccess.readSysFs(getTracingDir()
                    + "available_events");
            // Sort to facilitate binary search later
            Arrays.sort(eventNames);
        }

        return eventNames;
    }

    /**
     * Returns list of available tracing event categories
     *
     * @return list of available event categories
     */
    public static String[] getAvailableEventCategories() {
        // Since sysfs/ftrace does not change that often after system reboot,
        // let's cache categories. Categories are subdirectories inside
        // events dir. To enable a category, we should modify the "enable"
        // file inside subdir.
        // XXX: It may change after module load
        // QUESTION: how often are modules loaded in Android after reboot?
        if (!cachedCategories) {
            File file = new File(getTracingDir() + "events");
            eventCategories = file.list(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return new File(dir, name).isDirectory();
                }
            });
            // Sort to facilitate binary search later
            Arrays.sort(eventCategories);
        }

        return eventCategories;
    }

    /**
     * Return trace contents
     *
     * @return trace contents
     */
    public static String[] getTrace() {
        return SysFsAccess.readSysFs(getTracingDir() + "trace_pipe");
    }

    /**
     * Set tracer based on name. Fail if tracer is not available
     *
     * @param tracerName tracer
     * @return whether set operation succeeded or not
     */
    public static boolean setTracer(String tracerName) {
        assert(tracerName != null);

        /* only set tracer if it's actually provided by the system */
        if (Arrays.binarySearch(getAvailableTracers(), tracerName) >= 0)
            return SysFsAccess.writeSysFs(getTracingDir() + "current_tracer",
                    tracerName);

        return false;
    }

    /**
     * Enables tracing event.
     *
     * @param eventName event name
     * @param enable    enable toggle
     * @return whether enable operation succeeded or not
     */
    public static boolean enableTracerEvent(String eventName, boolean enable) {
        /* make sure specifiec event exists. binarysearch return negative
        when key is not found */
        if (Arrays.binarySearch(getAvailableEvents(), eventName) >= 0) {
            String[] parse = eventName.split(":");
            return SysFsAccess.writeSysFs(getTracingDir() + "events/"
                    + parse[0] + "/" + parse[1] + "/enable", enable ? "1" : "0");
        }

        return false;
    }

    /**
     * Enables tracing event category,
     *
     * @param eventCategory event category
     * @param enable        enable toggle
     * @return whether enable operation succeeded or not
     */
    public static boolean enableTracerEventCategory(String eventCategory,
            boolean enable) {
        /* make sure the specified event exists. binarysearch return negative
         when key is not found */
        if (Arrays.binarySearch(getAvailableEventCategories(),
                eventCategory) >= 0) {
            return SysFsAccess.writeSysFs(getTracingDir() + "events/"
                    + eventCategory + "/enable", enable ? "1" : "0");
        }

        return false;
    }

    public static void main(String[] args) {
        String[] tracers = FtraceAccess.getAvailableTracers();
        System.out.print("Available tracers: ");

        for (String tracer : tracers) {
            System.out.print(tracer + " ");
        }

        String[] events = FtraceAccess.getAvailableEvents();
        System.out.println("\nAvailable events: ");

        for (String event : events) {
            System.out.println(event);
        }

        String tracer = FtraceAccess.getTracer();
        System.out.println("Current tracer: " + tracer);

        String[] eventCategories = FtraceAccess.getAvailableEventCategories();
        System.out.print("Available event categories: ");
        for (String category : eventCategories) {
            System.out.print(category + " ");
        }

        FtraceAccess.setTracer("blk");
        FtraceAccess.enableTracerEventCategory("power", true);
        FtraceAccess.enableTracerEvent("udp:udp_fail_queue_rcv_skb", true);
        FtraceAccess.enableTracing(true);

        // Sleep for a while to catch traces
        try {
            Thread.sleep(500);
        } catch(InterruptedException e) {
            e.printStackTrace();
        }

        FtraceAccess.enableTracing(false);
        String[] trace = FtraceAccess.getTrace();

        System.out.println("Trace:");
        for (String line : trace) {
            System.out.println(line);
        }

        // Restore configurations
        FtraceAccess.enableTracerEvent("udp:udp_fail_queue_rcv_skb", false);
        FtraceAccess.enableTracerEventCategory("power", false);
        FtraceAccess.setTracer("nop");
    }
}
