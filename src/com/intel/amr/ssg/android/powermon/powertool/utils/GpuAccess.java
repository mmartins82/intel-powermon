package com.intel.amr.ssg.android.powermon.powertool.utils;

/**
 * User: Marcelo Martins <martins@cs.brown.edu>
 * Date: 12/30/13
 * Time: 9:13 AM
 * Java interface for access to GPU information via sysfs
 */
public class GpuAccess {

    private static String GPU_SYSFS_ROOT = "/sys/class/drm/card0/";

    static String getGpuDir() {
        return GPU_SYSFS_ROOT;
    }

    /**
     * Return residency time on RC6 GPU state
     * @return time (ms) on RC6 state residency
     */
    public static String getRc6Residency() {
        String line = SysFsAccess.readSysFsLine(getGpuDir()
                + "power/rc6_residency_ms", 0);
        return line.trim();
    }

    /**
     * Return residency time on RC6P GPU state
     * @return time (ms) on RC6P state residency
     */
    public static String getRc6pResidency() {
        String line = SysFsAccess.readSysFsLine(getGpuDir()
                + "power/rc6p_residency_ms", 0);
        return line.trim();
    }

    /**
     * Return residency time on RC6PP GPU state
     * @return time (ms) on RC6PP state residency
     */
    public static String getRc6ppResidency() {
        String line = SysFsAccess.readSysFsLine(getGpuDir()
                + "power/rc6pp_residency_ms", 0);
        return line.trim();
    }

    public static void main(String[] args) {
        System.out.println(String.format("RC6 Residency: %s",
                GpuAccess.getRc6Residency()));
        System.out.println(String.format("RC6P Residency: %s",
                GpuAccess.getRc6pResidency()));
        System.out.println(String.format("RC6PP Residency: %s",
                GpuAccess.getRc6ppResidency()));
    }
}
