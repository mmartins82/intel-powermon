package com.intel.amr.ssg.android.powermon.powertool.utils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

/**
 * User: Marcelo Martins <martins@cs.brown.edu>
 * Date: 12/23/13
 * Time: 10:59 AM
 */
public class SysFsAccess {
    /**
     * Read contents of sysfs file
     *
     * @param filename  file name
     * @return file lines as array
     */
    static String[] readSysFs(String filename) {
        assert(filename != null);

        List<String> contents = FileUtils.readFile(filename);
        return contents.toArray(new String[contents.size()]);
    }

    /**
     * Return line contents of sysfs file
     *
     * @param filename  sysfs file name
     * @param lineNum   line number
     * @return line read from sysfs file
     */
    static String readSysFsLine(String filename, int lineNum) {
        assert(filename != null);
        assert(lineNum >= 0);

        int i = 0;
        String line;

        try {
            BufferedReader fin = new BufferedReader(new InputStreamReader(new
                    FileInputStream(filename)), 256);

            for (line = fin.readLine(); i < lineNum && line != null; line =
                    fin.readLine()) {
                i++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "";
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }

        // Return empty array if requested line number does not exist
        if (i > lineNum)
            return "";

        return line;
    }

    /**
     * Write value to sysfs file
     *
     * @param filename  sysfs file name
     * @param value     value as string
     * @return whether write succeeded or not
     */
    static boolean writeSysFs(String filename, String value) {
        assert(filename != null);
        assert(value != null);

        /* newspace is needed at the end of string to persist sysfs change */
        return FileUtils.writeFile(filename, value + "\n");
    }
}
